# -*- coding: utf-8 -*-
from selenium import webdriver
import unittest
import time
import cases
from random import randint
from selenium.webdriver import ActionChains
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

lead_field = "lead" + str(randint(0, 1000))


class TestLeadField(unittest.TestCase):

    def setUp(self):
        self.driver = webdriver.Firefox(options=cases.options)
        self.driver.implicitly_wait(3)

    def test_create_lead_field(self):
        try:
            driver = self.driver
            cases.login(driver)
            driver.get("http://{}/crm/index.php?codmodulo=502"
                       .format(cases.IP))
            driver.find_element_by_id("btn_novo").click()
            WebDriverWait(driver, 10).until(
                EC.visibility_of_element_located(
                    (By.ID, "tamanho"))
            )
            driver.find_element_by_id("label").send_keys(lead_field)
            driver.find_element_by_xpath(
                "//div[@id='lbl_campo']/dd/div/div/a").click()
            driver.find_element_by_link_text("e_monetario4").click()
            driver.find_element_by_xpath(
                "//div[@id='lbl_tipo']/dd/div/div/a").click()
            driver.find_element_by_link_text("Text").click()
            driver.find_element_by_id("tamanho").send_keys("20")
            driver.find_element_by_id("ordem").send_keys("20")
            driver.find_element_by_id("lbl_exibe").click()
            imgA = __name__+"A.png"
            driver.save_screenshot(imgA)
            time.sleep(1)
            driver.find_element_by_id("btn_gravar").click()
            time.sleep(2)
            WebDriverWait(driver, 10).until(
                EC.invisibility_of_element_located(
                    (By.CLASS_NAME, "blockUI blockOverlay"))
            )
            driver.find_element_by_id("pesq_buscar").send_keys("e_monetario4")
            driver.find_element_by_id("btn_pesquisar").click()
            time.sleep(2)
            lead_field_assert = driver.find_element_by_id("registros_listar")\
                .find_element_by_tag_name("tbody")\
                .find_elements_by_tag_name("tr")
            for l in lead_field_assert:
                if lead_field in l.text:
                    action = ActionChains(driver)
                    action.double_click(l)
                    action.perform()
                    break
            WebDriverWait(driver, 10).until(
                EC.visibility_of_element_located(
                    (By.ID, "tamanho"))
            )
            imgB = __name__+"B.png"
            driver.save_screenshot(imgB)
        except Exception as e:
            driver.save_screenshot(cases.print_error(__name__))
            cases.logging.error(__name__ + str(e))
        try:
            res = cases.mse(self, imgA, imgB)
            self.assertAlmostEqual(0.0, float(res), delta=15.0)
        except AssertionError as e:
            driver.save_screenshot(cases.print_error(__name__))
            cases.logging.error(__name__ + str(e))
            raise AssertionError(str(e))

    def test_remove_lead_field(self):
        try:
            driver = self.driver
            cases.login(driver)
            driver.get("http://{}/crm/index.php?codmodulo=502"
                       .format(cases.IP))
            driver.find_element_by_id("pesq_buscar").send_keys("e_monetario4")
            driver.find_element_by_id("btn_pesquisar").click()
            time.sleep(2)
            lead_field_assert = driver.find_element_by_id("registros_listar")\
                .find_element_by_tag_name("tbody")\
                .find_elements_by_tag_name("tr")[2]\
                .find_elements_by_tag_name("td")[-1]
            action = ActionChains(driver)
            action.double_click(lead_field_assert)
            action.perform()
            WebDriverWait(driver, 10).until(
                EC.visibility_of_element_located(
                    (By.ID, "lbl_tamanho"))
            )
            driver.find_element_by_id("btn_deletar").click()
            driver.find_element_by_css_selector(
                ".ui-button.ui-widget.ui-state-default.ui-corner-all.ui-button-text-only")\
                .click()
            time.sleep(2)
            WebDriverWait(driver, 10).until(
                EC.invisibility_of_element_located(
                    (By.CLASS_NAME, "blockUI blockOverlay"))
            )
            driver.find_element_by_id("pesq_buscar").clear()
            driver.find_element_by_id("pesq_buscar").send_keys("e_monetario4")
            driver.find_element_by_id("btn_pesquisar").click()
            time.sleep(2)
        except Exception as e:
            driver.save_screenshot(cases.print_error(__name__))
            cases.logging.error(__name__ + str(e))
        try:
            res = False
            lead_field_assert = driver.find_element_by_id("registros_listar")\
                .find_element_by_tag_name("tbody")\
                .find_elements_by_tag_name("tr")[2]\
                .find_elements_by_tag_name("td")[-1]
            if lead_field in lead_field_assert.text:
                res = True
            self.assertFalse(res)
        except AssertionError as e:
            driver.save_screenshot(cases.print_error(__name__))
            cases.logging.error(__name__ + str(e))
            raise AssertionError(str(e))

    def tearDown(self):
        self.driver.quit()


if __name__ == "__main__":
    unittest.main()
