# -*- coding: utf-8 -*-
from selenium import webdriver
import unittest
import cases
from random import randint
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

queue = str(randint(0, 999)).zfill(3)


class TestQueue(unittest.TestCase):

    def setUp(self):
        self.driver = webdriver.Firefox(options=cases.options)
        self.driver.implicitly_wait(3)

    def test_create_queue(self):
        driver = self.driver
        try:
            cases.login(driver)
            driver.get("http://{}/crm/index.php?codmodulo=341"
                       .format(cases.IP))
            driver.find_element_by_xpath(
                "//button[@id='btn_novo']/span/span").click()
            driver.find_element_by_xpath(
                "//div[@id='lbl_tipo_fila']/dd/div/div/a").click()
            driver.find_element_by_xpath(
                "(//a[contains(text(),'Receptivo')])[2]").click()
            driver.find_element_by_id("fila").send_keys(queue)
            driver.find_element_by_id("descricao").send_keys(queue)
            driver.find_element_by_xpath(
                "//div[@id='lbl_ringinuse']/dd/div/div/a").click()
            driver.find_element_by_link_text(u"Não").click()
            driver.find_element_by_id("timeout").send_keys("60")
            driver.find_element_by_id("timealert").send_keys("15")
            driver.find_element_by_xpath(
                "//form[@id='frmHome']/fieldset[2]").click()
            driver.find_element_by_id("retry").send_keys("10")
            driver.find_element_by_xpath(
                "//div[@id='lbl_id_musica_espera']/dd/div/div/a").click()
            driver.find_element_by_link_text("Agente").click()
            driver.find_element_by_xpath(
                "//button[@id='btn_gravar']/span/span").click()
            WebDriverWait(driver, 10).until(
                EC.invisibility_of_element_located(
                    (By.CLASS_NAME, "blockUI blockOverlay"))
            )
            driver.find_element_by_id(
                "btn_canceledit").click()
            WebDriverWait(driver, 10).until(
                EC.invisibility_of_element(
                    (By.CLASS_NAME, "blockUI blockOverlay"))
            )
            driver.find_element_by_id("f_fila").send_keys("20"+queue)
            driver.find_element_by_id("btn_pesquisar").click()
            WebDriverWait(driver, 10).until(
                EC.text_to_be_present_in_element(
                    (By.CLASS_NAME, "ui-paging-info"), "Ver 1 - 1 de 1")
            )
            queue_assert = driver.find_element_by_css_selector(
                "#registros_listar > tbody:nth-child(1)")\
                .find_elements_by_tag_name("tr")[1]\
                .find_elements_by_tag_name("td")[1].get_attribute("innerHTML")
        except Exception as e:
            driver.save_screenshot(cases.print_error(__name__))
            cases.logging.error(__name__ + str(e))
        try:
            self.assertEqual("20"+queue, queue_assert)
        except AssertionError as e:
            driver.save_screenshot(cases.print_error(__name__))
            cases.logging.error(__name__ + str(e))
            raise AssertionError(str(e))

    def tearDown(self):
        self.driver.quit()


if __name__ == "__main__":
    unittest.main()
