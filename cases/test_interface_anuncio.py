from selenium import webdriver
import unittest
import time
import cases
from random import randint
from selenium.webdriver import ActionChains


anuncio = "anuncio"+str(randint(0, 1000)).zfill(4)


class TestAnuncio(unittest.TestCase):
    def setUp(self):
        self.driver = webdriver.Firefox(options=cases.options)
        self.driver.implicitly_wait(3)

    def test_create_and_delete_anuncio(self):
        try:
            driver = self.driver
            cases.login(driver)
            driver.get("http://{}/crm/index.php?codmodulo=401".format(cases.IP))
            time.sleep(2)
            # Salvando o estado inicial da grid
            driver.save_screenshot(__name__+'A1.png')
            driver.find_element_by_xpath('//*[@id="btn_novo"]').click()
            driver.find_element_by_xpath(
                '//*[@id="anuncio"]').send_keys(anuncio)
            cases.campos_select(driver, '/html/body/div[3]/div[2]/div[1]/div[2]/form/fieldset[2]/dl/div[3]/dd/div/div/a',
                                '/html/body/div[3]/div[2]/div[1]/div[2]/form/fieldset[2]/dl/div[3]/dd/div/ul/li[7]/a')
            cases.campos_select(driver, '/html/body/div[3]/div[2]/div[1]/div[2]/form/fieldset[2]/dl/div[4]/dd/div/div/a',
                                '/html/body/div[3]/div[2]/div[1]/div[2]/form/fieldset[2]/dl/div[4]/dd/div/ul/li[7]/a')
            cases.campos_select(driver, '/html/body/div[3]/div[2]/div[1]/div[2]/form/fieldset[2]/dl/div[5]/dd/div/div/a',
                                '/html/body/div[3]/div[2]/div[1]/div[2]/form/fieldset[2]/dl/div[5]/dd/div/ul/li[10]/a')
            driver.find_element_by_xpath("//div[@id='lbl_destino_tipo']/dd/div/div/a").click()
            driver.find_element_by_xpath("(//a[contains(text(),'Tronco')])[2]").click()
            time.sleep(1)
            driver.find_element_by_xpath("//div[@id='lbl_id_tronco_in']/dd/div/div/a").click()
            driver.find_element_by_link_text("trkfixo").click()
            driver.find_element_by_id("destino_exten").send_keys("123")
            driver.find_element_by_xpath(
                '/html/body/div[3]/div[2]/div[1]/div[2]/form/fieldset[2]/dl/div[25]/dt/span/a').click()
            driver.find_element_by_xpath(
                '/html/body/div[3]/div[2]/div[1]/div[2]/form/fieldset[2]/dl/div[26]/dt/span/a').click()
            driver.find_element_by_xpath('//*[@id="btn_gravar"]').click()
            time.sleep(4)
            grid_element_assert = driver.find_element_by_xpath(
                "/html/body/div[3]/div[2]/div[1]/div[1]/div/div/div[3]/div[3]/div/table/tbody/tr[3]/td[3]")
            action = ActionChains(self.driver)
            action.double_click(grid_element_assert)
            action.perform()
            time.sleep(1)
            driver.find_element_by_xpath('//*[@id="btn_deletar"]').click()
            time.sleep(1)
            driver.find_element_by_css_selector(".ui-button.ui-widget.ui-state-default.ui-corner-all.ui-button-text-only").click()
            time.sleep(3)
            # Salvando segundo print após o delete
            driver.save_screenshot(__name__+'B1.png')
        except Exception as e:
            driver.save_screenshot(cases.print_error(__name__))
            cases.logging.error(__name__ + str(e))
        try:
            res = cases.mse(self, 'test_interface_anuncioA1.png',
                            'test_interface_anuncioB1.png')
            # Comparando os print tirado antes do processo de criação/delete iniciar e depois de finalizado
            self.assertAlmostEqual(0.0, float(res), delta=20.0)
        except AssertionError as e:
            driver.save_screenshot(cases.print_error(__name__))
            cases.logging.error(__name__ + str(e))
            raise AssertionError(str(e))

    def tearDown(self):
        self.driver.quit()


if __name__ == "__main__":
    unittest.main()
