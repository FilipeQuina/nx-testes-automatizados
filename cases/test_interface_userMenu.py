# -*- coding: utf-8 -*-
# -*- coding: utf-8 -*-
from selenium import webdriver
import unittest
import cases
import time
from random import randint
from selenium.webdriver import ActionChains
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

grupo = "grupo" + str(randint(0, 1000))


class TestUserMenu(unittest.TestCase):

    def setUp(self):
        self.driver = webdriver.Firefox(options=cases.options)
        self.driver.implicitly_wait(3)

    def test_create_user_menu(self):
        driver = self.driver
        cases.login(driver)
        driver.get("http://{}/crm/index.php?codmodulo=28".format(cases.IP))
        driver.find_element_by_id("btn_novo").click()
        WebDriverWait(driver, 10).until(
            EC.visibility_of_element_located(
                (By.ID, "descricao"))
        )
        driver.find_element_by_id("descricao").send_keys(grupo)
        driver.find_element_by_xpath(
            "//div[@id='lbl_habilitar']/dd/div/div/a").click()
        driver.find_element_by_link_text("Sim").click()
        driver.find_element_by_xpath(
            "//div[@id='lbl_nivel']/dd/div/div/a").click()
        driver.find_element_by_link_text(u"4 (Direção)").click()
        driver.find_element_by_id("btn_gravar").click()

        WebDriverWait(driver, 10).until(
            EC.invisibility_of_element(
                (By.CLASS_NAME, "blockUI blockOverlay"))
        )
        driver.find_element_by_id("pesq_buscar").send_keys(grupo)
        action = ActionChains(self.driver)
        action.double_click()
        action.perform()
        time.sleep(2)
        driver.find_element_by_id("btn_pesquisar").click()
        WebDriverWait(driver, 10).until(
            EC.text_to_be_present_in_element(
                (By.CLASS_NAME, "ui-paging-info"), "Ver 1 - 1 de 1")
        )
        grupo_assert = driver.find_element_by_css_selector(
            "#registros_listar > tbody:nth-child(1)")\
            .find_elements_by_tag_name("tr")[1]\
            .find_elements_by_tag_name("td")[1]\
            .get_attribute("innerHTML")
        try:
            self.assertEqual(grupo, grupo_assert)
        except AssertionError as e:
            driver.save_screenshot(cases.print_error(__name__))
            cases.logging.error(
                "Ocorreu algum erro ao criar uma pausa: " + str(e))
            raise AssertionError(
                "Ocorreu algum erro ao criar uma pausa:" + str(e))

    def test_edit_menu_group(self):
        driver = self.driver
        try:
            cases.login(driver)
            driver.get("http://{}/crm/index.php?codmodulo=28".format(cases.IP))
            driver.find_element_by_id("pesq_buscar").send_keys(grupo)
            driver.find_element_by_id("btn_pesquisar").click()
            element = driver.find_element_by_id("registros_listar")\
                .find_element_by_tag_name("tbody")\
                .find_elements_by_tag_name("tr")[1]\
                .find_elements_by_tag_name("td")[1]
            action = ActionChains(self.driver)
            action.double_click(element)
            action.perform()
            time.sleep(3)
            driver.find_element_by_xpath(
                "//div[@id='lbl_habilitar']/dd/div/div/a").click()
            driver.find_element_by_link_text(u"Não").click()
            driver.find_element_by_id("btn_gravar").click()
            time.sleep(2)
            WebDriverWait(driver, 10).until(
                EC.visibility_of_element_located(
                    (By.ID, "registros_listar")
                )
            )
            res = driver.find_element_by_id("registros_listar")\
                .find_element_by_tag_name("tbody")\
                .find_elements_by_tag_name("tr")[1]\
                .find_elements_by_tag_name("td")[2]\
                .get_attribute("innerHTML")
        except Exception as e:
            driver.save_screenshot(cases.print_error(__name__))
            cases.logging.error(__name__ + str(e))
        try:
            self.assertEqual(res, "Não")
        except AssertionError as e:
            driver.save_screenshot(cases.print_error(__name__))
            cases.logging.error(__name__ + str(e))
            raise AssertionError(str(e))

    def tearDown(self):
        self.driver.quit()


if __name__ == "__main__":
    unittest.main()
