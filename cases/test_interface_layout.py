# -*- coding: utf-8 -*-
from selenium import webdriver
from selenium.webdriver.support.ui import Select
import unittest
import cases
import time
from random import randint
from selenium.webdriver import ActionChains
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

import os

layout = "layout" + str(randint(0, 1000))


class TestLayout(unittest.TestCase):

    def setUp(self):
        self.driver = webdriver.Firefox(options=cases.options)
        self.driver.implicitly_wait(3)
        self.driver.maximize_window()

    def drag_and_drop_layout(self, element):
        self.driver.execute_script("arguments[0].scrollIntoView();", element)
        time.sleep(1)
        self.driver.execute_script("arguments[0].scrollIntoView();", self.driver.find_element_by_id("cadLayout"))
        action = ActionChains(self.driver)
        action.drag_and_drop_by_offset(element, -300, 40)
        action.perform()

    def test_create_layout(self):
        try:
            driver = self.driver
            cases.login(driver)
            driver.get("http://{}/crm/index.php?codmodulo=126".format(cases.IP))
            driver.find_element_by_id("cadLayout").click()
            driver.find_element_by_id("descricao_layout").send_keys(layout)
            driver.find_element_by_id("arquivo3").send_keys(os.path.abspath("teste.csv"))
            driver.find_element_by_id("btnImportar3").click()
            cpf = driver.find_element_by_css_selector(
                "div.portlet:nth-child(4) > div:nth-child(1) > span:nth-child(2)")
            self.drag_and_drop_layout(cpf)
            nome = driver.find_element_by_css_selector(
                "#camposLayout2 > div:nth-child(2) > div:nth-child(1) > span:nth-child(2)")
            self.drag_and_drop_layout(nome)
            ddd = driver.find_element_by_css_selector(
                "div.portlet:nth-child(43) > div:nth-child(1) > span:nth-child(2)")
            self.drag_and_drop_layout(ddd)
            tel = driver.find_element_by_css_selector(
                "div.portlet:nth-child(43) > div:nth-child(1) > span:nth-child(2)")
            self.drag_and_drop_layout(tel)
            e_string = driver.find_element_by_css_selector(
                "div.portlet:nth-child(18) > div:nth-child(1) > span:nth-child(2)")
            self.drag_and_drop_layout(e_string)
            driver.find_element_by_id("salvarLayout").click()
            driver.find_element_by_css_selector(".swal2-confirm").click()

            driver.switch_to_alert().dismiss()
            time.sleep(2)
            select = Select(driver.find_element_by_name("layout"))
            select.select_by_index(1)
            WebDriverWait(driver, 10).until(
                EC.element_to_be_clickable(
                    (By.ID, "confLayout"))
            )
            driver.find_element_by_id("confLayout").click()
            layout_assert = len(driver.find_element_by_id(
                "camposLayout").find_elements_by_class_name("portlet"))
        except Exception as e:
            driver.save_screenshot(cases.print_error(__name__))
            cases.logging.error(__name__ + str(e))
        try:
            self.assertEqual(layout_assert, 5)
        except AssertionError as e:
            driver.save_screenshot(cases.print_error(__name__))
            cases.logging.error(__name__ + str(e))
            raise AssertionError(str(e))

    def test_remove_layout(self):
        try:
            driver = self.driver
            cases.login(driver)
            driver.get("http://{}/crm/index.php?codmodulo=126".format(cases.IP))
            WebDriverWait(driver, 10).until(
                EC.visibility_of_element_located(
                    (By.NAME, "layout"))
            )
            select = Select(driver.find_element_by_name("layout"))
            select.select_by_index(1)
            WebDriverWait(driver, 10).until(
                EC.element_to_be_clickable(
                    (By.ID, "confLayoutExcluir"))
            )
            driver.find_element_by_id("confLayoutExcluir").click()
            driver.find_element_by_css_selector(".swal2-confirm").click()
            lst_layout = []
            elements = driver.find_element_by_id("layout")\
                .find_elements_by_tag_name("option")
            for e in elements:
                lst_layout.append(e.get_attribute("innerHTML"))
        except Exception as e:
            driver.save_screenshot(cases.print_error(__name__))
            cases.logging.error(__name__ + str(e))
        try:
            self.assertTrue(layout is not lst_layout)
        except AssertionError as e:
            driver.save_screenshot(cases.print_error(__name__))
            cases.logging.error(__name__ + str(e))
            raise AssertionError(str(e))

    def tearDown(self):
        self.driver.quit()


if __name__ == "__main__":
    unittest.main()
