# -*- coding: utf-8 -*-
from selenium import webdriver
import cases
import unittest
from time import sleep
from random import randint
from selenium.webdriver import ActionChains
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
tronco = "tronco"+str(randint(0, 99999)).zfill(5)


class TestTrunk(unittest.TestCase):
    def setUp(self):
        self.driver = webdriver.Firefox(options=cases.options)
        self.driver.implicitly_wait(3)

    def test_create_trunk(self):
        try:
            driver = self.driver
            cases.login(driver)
            driver.get("http://{}/crm/index.php?codmodulo=347"
                       .format(cases.IP))
            driver.find_element_by_id("btn_novo").click()
            driver.find_element_by_xpath(
                "//div[@id='lbl_id_tipo_tronco']/dd/div/div/a").click()
            driver.find_element_by_xpath(
                "(//a[contains(text(),'KHOMP')])[2]").click()
            driver.find_element_by_id("tronco").send_keys(tronco)
            driver.find_element_by_id("descricao").send_keys(tronco)
            driver.find_element_by_xpath(
                "//div[@id='lbl_id_tipo_tronco']/dd/div/div/a").click()
            driver.find_element_by_xpath(
                "(//a[contains(text(),'SIP')])[3]").click()
            driver.find_element_by_xpath(
                "//button[@id='btn_gravar']/span/span").click()
            sleep(2)
            WebDriverWait(driver, 10).until(
                EC.invisibility_of_element_located(
                    (By.CLASS_NAME, "blockUI blockOverlay")
                )
            )
            driver.find_element_by_id("f_tronco").send_keys(tronco)
            driver.find_element_by_xpath(
                "//button[@id='btn_pesquisar']/span/span").click()
        except Exception as e:
            driver.save_screenshot(cases.print_error(__name__))
            cases.logging.error(__name__ + str(e))
        try:
            sleep(2)
            self.assertEqual("Ver 1 - 1 de 1",
                             driver.find_element_by_xpath(
                                 "//td[@id='registros_pager_right']/div").text)
        except AssertionError as e:
            driver.save_screenshot(cases.print_error(__name__))
            cases.logging.error(__name__ + str(e))
            raise AssertionError(str(e))

    def test_remove_trunk(self):
        try:
            driver = self.driver
            cases.login(driver)
            driver.get("http://{}/crm/index.php?codmodulo=347"
                       .format(cases.IP))
            driver.find_element_by_id("f_tronco").send_keys(tronco)
            driver.find_element_by_id("btn_pesquisar").click()
            sleep(1)
            el = driver.find_element_by_id("registros_listar")\
                .find_element_by_tag_name("tbody")\
                .find_elements_by_tag_name("tr")[1]
            action = ActionChains(driver)
            action.double_click(el)
            action.perform()
            WebDriverWait(driver, 10).until(
                EC.visibility_of_element_located(
                    (By.ID, "tronco")
                )
            )
            driver.find_element_by_xpath(
                "//div[@id='lbl_habilitar']/dd/label").click()
            driver.find_element_by_xpath(
                "//button[@id='btn_gravar']/span/span").click()
            sleep(2)
            WebDriverWait(driver, 10).until(
                EC.invisibility_of_element_located(
                    (By.CLASS_NAME, 'blockUI blockOverlay')
                )
            )
        except Exception as e:
            driver.save_screenshot(cases.print_error(__name__))
            cases.logging.error(__name__ + str(e))
        try:
            res = driver.find_element_by_class_name("ui-paging-info").text
            self.assertEqual(res, "Nenhum registro para visualizar")
        except AssertionError as e:
            driver.save_screenshot(cases.print_error(__name__))
            cases.logging.error(__name__ + str(e))
            raise AssertionError(str(e))

    def tearDown(self):
        self.driver.quit()


if __name__ == "__main__":
    unittest.main()
