# -*- coding: utf-8 -*-
from selenium import webdriver
import cases
import unittest
import time
from random import randint
from selenium.webdriver import ActionChains
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

camp = "cmp" + str(randint(0, 99999)).zfill(3)


class TestCampaign(unittest.TestCase):
    def setUp(self):
        self.driver = webdriver.Firefox(options=cases.options)
        self.driver.implicitly_wait(3)

    def test_create_campaign(self):
        try:
            driver = self.driver
            cases.login(driver)
            driver.get("http://{}/crm/index.php?codmodulo=123"
                       .format(cases.IP))
            driver.find_element_by_xpath(
                "//button[@id='btn_novo']/span/span").click()
            driver.find_element_by_xpath(
                "//div[@id='lbl_id_tipo_campanha']/div/div/a").click()
            driver.find_element_by_xpath(
                "(//a[contains(text(),'LEAD')])[3]").click()
            driver.find_element_by_xpath(
                "//div[@id='lbl_fila']/div/div/a").click()
            driver.find_element_by_xpath(
                "(//a[contains(text(),'10001')])[2]").click()
            driver.find_element_by_id("tentativas_mensal").clear()
            driver.find_element_by_id("tentativas_mensal").send_keys("10")
            driver.find_element_by_id("nome_campanha").send_keys(camp)
            driver.find_element_by_id("btn_agentes").click()
            driver.find_element_by_id("lookup_id_agentes").click()
            driver.find_element_by_id("gs_desc").click()
            time.sleep(2)
            driver.find_element_by_xpath("//tr[@id='4']/td[4]/span").click()
            driver.find_element_by_id("chk_id_agentes_4").click()
            driver.find_element_by_xpath(
                "//button[@id='d_btn_sel_id_agentes']/span").click()
            WebDriverWait(driver, 10).until(
                EC.invisibility_of_element(
                    (By.CLASS_NAME, "blockUI blockOverlay"))
            )
            driver.find_element_by_id("btn_horario").click()
            driver.find_element_by_id("data_inicio").click()
            driver.find_element_by_id("data_inicio").send_keys("01/01/2020")
            driver.find_element_by_id("data_termino").click()
            driver.find_element_by_id("data_termino").send_keys("01/01/2050")
            driver.find_element_by_id("btn_gravar").click()
            time.sleep(2)
            WebDriverWait(driver, 10).until(
                EC.invisibility_of_element(
                    (By.CLASS_NAME, "blockUI blockOverlay"))
            )
            driver.find_element_by_id("pesq_buscar").send_keys(camp)
            camp_assert = driver.find_element_by_id("registros_listar")\
                .find_element_by_tag_name("tbody")\
                .find_elements_by_tag_name("tr")[1]\
                .find_elements_by_tag_name("td")[5]\
                .get_attribute("innerHTML")
        except Exception as e:
            driver.save_screenshot(cases.print_error(__name__))
            cases.logging.error(__name__ + str(e))
        try:
            self.assertEqual(camp, camp_assert)
        except AssertionError as e:
            driver.save_screenshot(cases.print_error(__name__))
            cases.logging.error(__name__ + str(e))
            raise AssertionError(str(e))

    def test_remove_campaign(self):
        try:
            driver = self.driver
            cases.login(driver)
            driver.get("http://{}/crm/index.php?codmodulo=123"
                       .format(cases.IP))
            WebDriverWait(driver, 20).until(
                EC.visibility_of_element_located(
                    (By.ID, "registros_listar")
                )
            )
            el = driver.find_element_by_id("registros_listar")\
                .find_element_by_tag_name("tbody")\
                .find_elements_by_tag_name("tr")[1]
            action = ActionChains(driver)
            action.double_click(el)
            action.perform()
            WebDriverWait(driver, 10).until(
                EC.visibility_of_element_located(
                    (By.ID, "nome_campanha")))
            driver.find_element_by_css_selector(
                "#lbl_id_status_campanha > div:nth-child(2) "
                + "> div:nth-child(1) > a:nth-child(2)").click()
            driver.find_element_by_css_selector(
                "#lbl_id_status_campanha "
                + "> div:nth-child(2) > ul:nth-child(2) > li:nth-child(6) "
                + "> a:nth-child(1)")\
                .click()
            driver.find_element_by_id("btn_gravar").click()
            time.sleep(3)
            WebDriverWait(driver, 10).until(
                EC.invisibility_of_element(
                    (By.CLASS_NAME, "blockUI blockOverlay"))
            )
            driver.find_element_by_id("pesq_buscar").send_keys(camp)
            driver.find_element_by_id("btn_pesquisar").click()
        except Exception as e:
            driver.save_screenshot(cases.print_error(__name__))
            cases.logging.error(__name__ + str(e))
        try:
            WebDriverWait(driver, 10).until(
                EC.text_to_be_present_in_element(
                    (By.CLASS_NAME, "ui-paging-info"),
                    "Nenhum registro para visualizar"
                )
            )
            res = driver.find_element_by_class_name("ui-paging-info").text
            self.assertEqual(res, "Nenhum registro para visualizar")
        except AssertionError as e:
            driver.save_screenshot(cases.print_error(__name__))
            cases.logging.error(__name__ + str(e))
            raise AssertionError(str(e))

    def tearDown(self):
        self.driver.quit()


if __name__ == "__main__":
    unittest.main()
