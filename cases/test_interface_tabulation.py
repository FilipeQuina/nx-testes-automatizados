# -*- coding: utf-8 -*-
from selenium import webdriver
import unittest
import cases
from random import randint
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

tabulation = u"tabulacao" + str(randint(0, 1000))


class TestTabulation(unittest.TestCase):

    def setUp(self):
        self.driver = webdriver.Firefox(options=cases.options)
        self.driver.implicitly_wait(3)

    def test_create_tabulation(self):
        driver = self.driver
        try:
            cases.login(driver)
            driver.get("http://{}/crm/index.php?codmodulo=503".format(cases.IP))
            driver.find_element_by_xpath(
                "//button[@id='btn_novo']/span/span/img").click()
            driver.find_element_by_id("descricao").send_keys(tabulation)
            driver.find_element_by_xpath(
                "//dt[@id='boxchamada_manual']/span/a").click()
            driver.find_element_by_xpath(
                "//button[@id='btn_gravar']/span/span").click()
            WebDriverWait(driver, 10).until(
                EC.invisibility_of_element(
                    (By.CLASS_NAME, "blockUI blockOverlay"))
            )
            driver.find_element_by_id("pesq_buscar").send_keys(tabulation)
            driver.find_element_by_id("btn_pesquisar").click()
            WebDriverWait(driver, 10).until(
                EC.text_to_be_present_in_element(
                    (By.CLASS_NAME, "ui-paging-info"), "Ver 1 - 1 de 1")
            )
            tab_assert = driver.find_element_by_css_selector("#registros_listar > tbody:nth-child(1)")\
                .find_elements_by_tag_name("tr")[1]\
                .find_elements_by_tag_name("td")[1].get_attribute("innerHTML")
            self.assertEqual(tabulation, tab_assert)
        except AssertionError as e:
            driver.save_screenshot(cases.print_error(__name__))
            cases.logging.error(__name__ + str(e))
            raise AssertionError(str(e))

    def tearDown(self):
        self.driver.quit()


if __name__ == "__main__":
    unittest.main()
