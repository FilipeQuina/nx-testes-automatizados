# -*- coding: utf-8 -*-
from selenium import webdriver
import unittest
from time import sleep
import cases
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC


class TestCheckPass(unittest.TestCase):

    def setUp(self):
        self.driver = webdriver.Firefox(options=cases.options)
        self.driver.implicitly_wait(3)

    def test_check_pass(self):
        driver = self.driver
        cases.login(driver)
        driver.get("http://{}/crm/index.php?codmodulo=30".format(cases.IP))
        WebDriverWait(driver, 10).until(
            EC.visibility_of_element_located(
                (By.ID, "senhan"))
        )
        driver.find_element_by_id("senha").send_keys(cases.PASS)
        driver.save_screenshot(__name__+"A.png")
        driver.find_element_by_id("senhan").send_keys("1111111111")
        driver.find_element_by_id("senhac").send_keys("ewewewewewe")
        driver.find_element_by_id("btn_gravar").click()
        sleep(1)
        WebDriverWait(driver, 10).until(
            EC.invisibility_of_element_located(
                (By.CLASS_NAME, "blockUI blockOverlay"))
        )
        sleep(1)
        driver.save_screenshot(__name__+"B.png")
        try:
            self.assertAlmostEqual(0.0,
                                   cases.mse(self, __name__+"A.png", __name__+"B.png"), delta=180.0)
        except AssertionError as e:
            driver.save_screenshot(cases.print_error(__name__))
            cases.logging.error(
                "Ocorreu algum erro ao criar uma pausa: " + str(e))
            raise AssertionError(
                "Ocorreu algum erro ao criar uma pausa:" + str(e))

    def tearDown(self):
        self.driver.quit()


if __name__ == "__main__":
    unittest.main()
