# -*- coding: utf-8 -*-
from selenium import webdriver
import unittest
import cases
import time
from random import randint
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

import os

audio = "audio" + str(randint(0, 9999))


class TestAudio(unittest.TestCase):

    def setUp(self):
        self.driver = webdriver.Firefox(options=cases.options)
        self.driver.implicitly_wait(3)
        self.driver.maximize_window()

    def test_create_audio(self):
        try:
            driver = self.driver
            cases.login(driver)
            driver.get("http://{}/crm/index.php?codmodulo=143"
                       .format(cases.IP))
            driver.find_element_by_id("btn_novo").click()
            driver.find_element_by_id("descricao").send_keys(audio)
            driver.find_element_by_id("tempo").send_keys("20")
            driver.find_element_by_id("lbl_habilitado").click()
            driver.find_element_by_id("btn_upload").click()
            driver.find_element_by_id("arquivo_audio")\
                .send_keys(os.path.abspath("audio.mp3"))
            time.sleep(2)
            driver.find_element_by_css_selector(
                ".ui-button.ui-widget.ui-state-default.ui-corner-all.ui-button-text-only").click()
            time.sleep(2)
            WebDriverWait(driver, 10).until(
                EC.invisibility_of_element_located(
                    (By.CLASS_NAME, "blockUI blockOverlay"))
            )
            driver.find_element_by_id("btn_gravar").click()
            WebDriverWait(driver, 10).until(
                EC.element_to_be_clickable(
                    (By.ID, "btn_pesquisar"))
            )
            WebDriverWait(driver, 10).until(
                EC.invisibility_of_element_located(
                    (By.CLASS_NAME, "blockUI blockOverlay"))
            )
            driver.find_element_by_id("pesq_buscar").send_keys(audio)
            driver.find_element_by_id("btn_pesquisar").click()
            WebDriverWait(driver, 10).until(
                EC.text_to_be_present_in_element(
                    (By.CLASS_NAME, "ui-paging-info"), "Ver 1 - 1 de 1")
            )
            element_assert = driver.find_element_by_id("registros_listar")\
                .find_element_by_tag_name("tbody")\
                .find_elements_by_tag_name("tr")[1]\
                .find_elements_by_tag_name("td")[1]\
                .get_attribute("innerHTML")
        except Exception as e:
            driver.save_screenshot(cases.print_error(__name__))
            cases.logging.error(__name__ + str(e))
        try:
            self.assertEqual(element_assert, audio)
        except AssertionError as e:
            driver.save_screenshot(cases.print_error(__name__))
            cases.logging.error(__name__ + str(e))
            raise AssertionError(str(e))

    def tearDown(self):
        self.driver.quit()


if __name__ == "__main__":
    unittest.main()
