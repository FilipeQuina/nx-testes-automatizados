from selenium import webdriver
import cases
import unittest
import time
import random
from selenium.webdriver import ActionChains
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.keys import Keys
randomStore = str(random.randint(1, 1000)).zfill(4)


class TestStores(unittest.TestCase):
    def setUp(self):
        self.driver = webdriver.Firefox(options=cases.options)
        self.driver.implicitly_wait(10)

    def test_create_store(self):
        try:

            driver = self.driver
            cases.login(driver)
            driver.get("http://{}/crm/index.php?codmodulo=3"
                       .format(cases.IP))
            # Novo Cadastro Loja
            time.sleep(3)
            driver.find_element_by_xpath(
                '//*[@id="btn_novo"]/span/span').click()
            driver.find_element_by_id('descricao').send_keys(
                "Descrição Loja Teste{}".format(randomStore))
            driver.find_element_by_xpath(
                '/html/body/div[3]/div[2]/div[1]/div[2]/form[1]/fieldset[2]/div[3]/dd/div/div/a').click()
            driver.find_element_by_xpath(
                '/html/body/div[3]/div[2]/div[1]/div[2]/form[1]/fieldset[2]/div[3]/dd/div/ul/li[1]/a').click()
            driver.find_element_by_id('pdv').send_keys('Código PDV Teste')
            driver.find_element_by_id('cep').send_keys('36090170')
            time.sleep(6)
            driver.find_element_by_id(
                'endereco').send_keys('Endereço Fictício')
            driver.find_element_by_id('numero').send_keys('123')
            driver.find_element_by_id('complemento').send_keys('Complemento')
            driver.find_element_by_id('cidade').send_keys(' ')
            driver.find_element_by_xpath(
                '/html/body/div[3]/div[2]/div[1]/div[2]/form[1]/fieldset[2]/div[11]/dd/div/div/a').click()
            driver.find_element_by_xpath(
                '/html/body/div[3]/div[2]/div[1]/div[2]/form[1]/fieldset[2]/div[11]/dd/div/ul/li[13]/a').click()
            driver.find_element_by_id('telefone1').send_keys('988575939')
            driver.find_element_by_id('telefone2').send_keys('988765432')
            driver.find_element_by_id('cnpj').send_keys('46.688.116/0001-66')
            driver.find_element_by_id('ie').send_keys('123456')
            driver.find_element_by_id('ddd').send_keys('32')
            driver.find_element_by_id('cnl').send_keys('CNL Fictício')
            driver.find_element_by_id('tag_loja').send_keys('LPL Loja')
            driver.find_element_by_id('arquivo_separar').send_keys('Teste')
            driver.find_element_by_id('arquivo_cliente').send_keys('Teste')
            driver.find_element_by_id('limite_importacao').send_keys('10')
            driver.find_element_by_id('qtde_colunas').send_keys('4 colunas')
            driver.find_element_by_xpath(
                '/html/body/div[3]/div[2]/div[1]/div[2]/form[1]/fieldset[2]/div[25]/dd/div/div/a').click()
            driver.find_element_by_xpath(
                '/html/body/div[3]/div[2]/div[1]/div[2]/form[1]/fieldset[2]/div[25]/dd/div/ul/li[12]/a').click()
            driver.find_element_by_xpath(
                '/html/body/div[3]/div[2]/div[1]/div[2]/form[1]/fieldset[2]/div[30]/dd/div/div/a').click()
            driver.find_element_by_xpath(
                '/html/body/div[3]/div[2]/div[1]/div[2]/form[1]/fieldset[2]/div[30]/dd/div/ul/li[3]/a').click()
            # Print "A"
            time.sleep(2)
            driver.save_screenshot(__name__+'a.png')
            # Gravando
            driver.find_element_by_xpath(
                '/html/body/div[3]/div[2]/div[1]/div[2]/form[1]/fieldset[1]/div[1]/button/span/span').click()
            time.sleep(5)
            # Segundo Print "B"
            driver.find_element_by_id("pesq_buscar").send_keys(
                "Descrição Loja Teste"+randomStore)
            driver.find_element_by_id("btn_pesquisar").click()
            grid_element_assert = driver.find_element_by_id("registros_listar")\
                .find_element_by_tag_name("tbody")\
                .find_elements_by_tag_name("tr")[1]\
                .find_elements_by_tag_name("td")[1]
            actions = ActionChains(driver)
            actions.double_click(grid_element_assert)
            actions.perform()
            WebDriverWait(driver, 10).until(
                EC.visibility_of_element_located((By.ID, "cep")))
            driver.save_screenshot(__name__ + 'b.png')
        except Exception as e:
            driver.save_screenshot(cases.print_error(__name__))
            cases.logging.error(__name__ + str(e))
        try:
            res = cases.mse(self, __name__ + 'a.png',
                            __name__ + 'b.png')
            self.assertAlmostEqual(0.0, float(res), delta=610.0)
        except AssertionError as e:
            driver.save_screenshot(cases.print_error(__name__))
            cases.logging.error(__name__ + str(e))
            raise AssertionError(str(e))

    def test_delete_store(self):
        try:
            driver = self.driver
            cases.login(driver)
            driver.get("http://{}/crm/index.php?codmodulo=3"
                       .format(cases.IP))
            driver.find_element_by_id("pesq_buscar").send_keys(
                "Descrição Loja Teste"+randomStore)
            driver.find_element_by_id("btn_pesquisar").click()
            WebDriverWait(driver, 10).until(
                EC.text_to_be_present_in_element(
                    (By.CLASS_NAME, "ui-paging-info"), "Ver 1 - 1 de 1")
            )
            grid_element = driver.find_element_by_id("registros_listar")\
                .find_element_by_tag_name("tbody")\
                .find_elements_by_tag_name("tr")[1]
            actions = ActionChains(driver)
            actions.double_click(grid_element)
            actions.perform()
            # Deletando
            WebDriverWait(driver, 10).until(
                EC.visibility_of_element_located(
                    (By.ID, "cep"))
            )
            driver.find_element_by_id(
                'btn_deletar').click()
            time.sleep(1)
            action = ActionChains(driver)
            action.key_down(Keys.ENTER)
            action.perform()
            time.sleep(1)
        except Exception as e:
            driver.save_screenshot(cases.print_error(__name__))
            cases.logging.error(__name__ + str(e))
        try:
            WebDriverWait(driver, 10).until(
                EC.visibility_of_element_located(
                    (By.CLASS_NAME, "ui-paging-info"))
            )
            self.assertEqual(
                "Nenhum registro para visualizar",
                driver.find_element_by_class_name("ui-paging-info").text)
        except AssertionError as e:
            driver.save_screenshot(cases.print_error(__name__))
            cases.logging.error(__name__ + str(e))
            raise AssertionError(str(e))

    def tearDown(self):
        self.driver.quit()

if __name__ == "__main__":
    unittest.main()
