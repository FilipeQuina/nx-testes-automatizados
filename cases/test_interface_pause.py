# -*- coding: utf-8 -*-
from selenium import webdriver
import unittest
import cases
import time
from random import randint
from selenium.webdriver import ActionChains
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

pause_name = "pausa" + str(randint(0, 1000)).zfill(4)


class TestPause(unittest.TestCase):

    def setUp(self):
        self.driver = webdriver.Firefox(options=cases.options)
        self.driver.implicitly_wait(3)

    def test_create_pause(self):
        driver = self.driver
        cases.login(driver)
        driver.get("http://{}/crm/index.php?codmodulo=115".format(cases.IP))
        driver.find_element_by_xpath(
            "//button[@id='btn_novo']/span/span").click()
        driver.find_element_by_id("pausa").send_keys(pause_name)
        driver.find_element_by_id("tempo").send_keys("1000")
        driver.find_element_by_xpath(
            "//div[@id='lbl_acao']/dd/div/div/a").click()
        driver.find_element_by_link_text(u"Nenhuma Ação").click()
        driver.find_element_by_xpath(
            "//div[@id='lbl_tipo']/dd/div/div/a").click()
        driver.find_element_by_link_text("Improdutiva").click()
        driver.find_element_by_id("lookup_cod_grupo").click()
        driver.find_element_by_id("chk_cod_grupo_6").click()
        driver.find_element_by_xpath(
            "//button[@id='d_btn_sel_cod_grupo']/span").click()
        driver.find_element_by_xpath(
            "//dt[@id='boxhabilitado']/span/a").click()
        driver.find_element_by_xpath("//dt[@id='boxpadrao']/span/a").click()
        driver.find_element_by_xpath("//dt[@id='boxagenda']/span/a").click()
        driver.find_element_by_xpath("//dt[@id='boxtabulacao']/span/a").click()
        driver.find_element_by_xpath(
            "//div[@id='lbl_atendimento']/dd/label").click()
        driver.find_element_by_xpath(
            "//dt[@id='boxpermite_chamada']/span/a").click()
        driver.find_element_by_xpath(
            "//div[@id='lbl_tabulacao_auto']/dd/div/div/span").click()
        driver.find_element_by_link_text(
            "Robo - Abandonada por falta de robo disponivel").click()
        driver.find_element_by_id("btn_gravar").click()
        WebDriverWait(driver, 10).until(
            EC.invisibility_of_element(
                (By.CLASS_NAME, "blockUI blockOverlay"))
        )
        driver.find_element_by_id("pesq_buscar").send_keys(pause_name)
        driver.find_element_by_id("btn_pesquisar").click()
        WebDriverWait(driver, 10).until(
            EC.text_to_be_present_in_element(
                (By.CLASS_NAME, "ui-paging-info"), "Ver 1 - 1 de 1")
        )
        pause_name_assert = driver.find_element_by_css_selector("#registros_listar > tbody:nth-child(1)")\
            .find_elements_by_tag_name("tr")[1]\
            .find_elements_by_tag_name("td")[1]\
            .get_attribute("innerHTML")
        try:
            self.assertEqual(pause_name, pause_name_assert)
        except AssertionError as e:
            driver.save_screenshot(cases.print_error(__name__))
            cases.logging.error(
                "Ocorreu algum erro ao criar uma pausa: " + str(e))
            raise AssertionError(
                "Ocorreu algum erro ao criar uma pausa: " + str(e))

    def test_edit_pause(self):
        driver = self.driver
        try:
            cases.login(driver)
            driver.get("http://{}/crm/index.php?codmodulo=115".format(cases.IP))
            driver.find_element_by_id("pesq_buscar").send_keys(pause_name)
            driver.find_element_by_id("btn_pesquisar").click()
            time.sleep(2)
            element = driver.find_element_by_id("registros_listar")\
                .find_element_by_tag_name("tbody")\
                .find_elements_by_tag_name("tr")[1]\
                .find_elements_by_tag_name("td")
            is_habilitado = element[3].text
            action = ActionChains(self.driver)
            action.double_click(element[1])
            action.perform()
            time.sleep(3)
            driver.find_element_by_xpath(
                "//dt[@id='boxhabilitado']/span/a").click()
            driver.find_element_by_id("btn_gravar").click()
            time.sleep(2)
            is_habilitado_assert = driver.find_element_by_id("registros_listar")\
                .find_element_by_tag_name("tbody")\
                .find_elements_by_tag_name("tr")[1]\
                .find_elements_by_tag_name("td")[3]\
                .get_attribute("innerHTML")
        except Exception as e:
            driver.save_screenshot(cases.print_error(__name__))
            cases.logging.error(__name__ + str(e))
        try:
            self.assertNotEqual(is_habilitado, is_habilitado_assert)
        except AssertionError as e:
            driver.save_screenshot(cases.print_error(__name__))
            cases.logging.error(__name__ + str(e))
            raise AssertionError(str(e))

    def tearDown(self):
        self.driver.quit()


if __name__ == "__main__":
    unittest.main()
