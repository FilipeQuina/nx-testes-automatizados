from selenium.webdriver.firefox.options import Options
import logging
from datetime import datetime
import os
import numpy as np
from cv2 import cv2

now = datetime.now()
bld = open("build.txt")
build = bld.read().replace('\n', "")
bld.close()
os.makedirs(build+'/logs/', mode="0o777", exist_ok=True)
os.makedirs(build+"/errors/", mode="0o777", exist_ok=True)

logging.basicConfig(
    filename=build+'/logs/'+str(now.day).zfill(2)+"-"+str(now.month).zfill(2)+"-"+str(now.year)+'.log',
    level=logging.ERROR, format='%(asctime)s %(levelname)s: %(message)s')


def print_error(name_file):
    return build+"/errors/"+str(now.year)+str(now.month)+str(now.day)+name_file+".png"


IP = "177.70.21.124"
PASS = "3rapHE-uyUxA"
options = Options()
options.log.level = "trace"
options.add_argument("--headless")


def mse(self, imageA, imageB):
    img1 = cv2.imread(imageA)
    img1 = cv2.cvtColor(img1, cv2.COLOR_BGR2GRAY)
    img2 = cv2.imread(imageB)
    img2 = cv2.cvtColor(img2, cv2.COLOR_BGR2GRAY)
    err = np.sum((img1.astype("float") - img2.astype("float")) ** 2)
    err /= float(img1.shape[0] * img1.shape[1])
    os.remove(imageA)
    os.remove(imageB)
    return err


def login(driver):
    driver.get("http://{}/crm/index.php".format(IP))
    driver.find_element_by_id("l_login").send_keys("suporte")
    driver.find_element_by_id("l_senha").send_keys(PASS)
    driver.find_element_by_css_selector(".btn,.btn-send").click()

# Função pra lidar com os campos select do sistema.


def campos_select(driver, xpath1, xpath2):
    driver.find_element_by_xpath(xpath1).click()
    driver.find_element_by_xpath(xpath2).click()
