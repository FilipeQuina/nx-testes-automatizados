# -*- coding: utf-8 -*-
from selenium import webdriver
import unittest
from time import sleep
import cases
from selenium.webdriver import ActionChains
from selenium.webdriver.common.by import By
from random import randint
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

template = "template" + str(randint(0, 1000))


class TestMessageTemplate(unittest.TestCase):

    def setUp(self):
        self.driver = webdriver.Firefox(options=cases.options)
        self.driver.implicitly_wait(3)

    def test_create_message_template_sms(self):
        try:
            driver = self.driver
            cases.login(driver)
            driver.get("http://{}/crm/index.php?codmodulo=509".format(cases.IP))

            driver.find_element_by_id("btn_novo").click()
            WebDriverWait(driver, 10).until(
                EC.visibility_of_element_located(
                    (By.ID, "description"))
            )
            sleep(2)
            driver.find_element_by_xpath(
                "//div[@id='lbl_id_tipo_mensagem']/div/div/a").click()
            driver.find_element_by_xpath(
                "(//a[contains(text(),'SMS')])[2]").click()
            driver.find_element_by_xpath(
                "//div[@id='lbl_id_layout_mailing']/div/div/a").click()
            driver.find_element_by_link_text("Campanha Fixa").click()
            driver.find_element_by_id("description").send_keys(template+"SMS")
            driver.find_element_by_id("conteudo").send_keys("ola")
            driver.find_element_by_xpath("(//span[@id='tags'])[2]").click()
            driver.find_element_by_id("conteudo").send_keys(
                u"seu cpf é")
            driver.find_element_by_xpath("(//span[@id='tags'])[1]").click()
            driver.find_element_by_xpath("(//span[@id='tags'])[3]").click()
            driver.find_element_by_xpath(
                "//button[@id='btn_gravar']/span/span").click()
            sleep(2)
            WebDriverWait(driver, 10).until(
                EC.invisibility_of_element_located(
                    (By.CLASS_NAME, "blockUI blockOverlay"))
            )
            driver.find_element_by_xpath(
                "//button[@id='btn_voltar']/span/span").click()
            WebDriverWait(driver, 10).until(
                EC.visibility_of_element_located(
                    (By.ID, "f_descricao"))
            )
            driver.find_element_by_id("f_descricao").send_keys(template+"SMS")
            driver.find_element_by_xpath(
                "//button[@id='btn_pesquisar']/span/span").click()
        except Exception as e:
            driver.save_screenshot(cases.print_error(__name__))
            cases.logging.error(__name__ + str(e))
        try:
            sleep(2)
            res = driver.find_element_by_class_name("ui-paging-info").text
            self.assertEqual(res, "Ver 1 - 1 de 1")
        except AssertionError as e:
            driver.save_screenshot(cases.print_error(__name__))
            cases.logging.error(__name__ + str(e))
            raise AssertionError(str(e))

    def test_remove_message_template_sms(self):
        try:
            driver = self.driver
            cases.login(driver)
            driver.get("http://{}/crm/index.php?codmodulo=509".format(cases.IP))
            driver.find_element_by_id("f_descricao").clear()
            driver.find_element_by_id("f_descricao").send_keys(template+"SMS")
            driver.find_element_by_xpath(
                "//button[@id='btn_pesquisar']/span/span").click()
            sleep(1)
            el = driver.find_element_by_id("registros_listar")
            el.find_element_by_tag_name("tbody")
            el.find_elements_by_tag_name("tr")[1]
            ActionChains(driver).double_click(el).perform()
            WebDriverWait(driver, 10).until(
                EC.visibility_of_element_located(
                    (By.ID, "description"))
            )
            sleep(2)
            driver.find_element_by_xpath(
                "//div[@id='lbl_enable']/div/div/a").click()
            driver.find_element_by_xpath(
                u"(//a[contains(text(),'Não')])[2]").click()
            driver.find_element_by_xpath(
                "//button[@id='btn_gravar']/span/span").click()
            sleep(2)
            WebDriverWait(driver, 10).until(
                EC.invisibility_of_element_located(
                    (By.CLASS_NAME, "blockUI blockOverlay"))
            )
            driver.find_element_by_xpath(
                "//button[@id='btn_voltar']/span/span").click()
            WebDriverWait(driver, 10).until(
                EC.visibility_of_element_located(
                    (By.ID, "f_descricao"))
            )
            driver.find_element_by_id("f_descricao").clear()
            driver.find_element_by_id("f_descricao").send_keys(template+"SMS")
            driver.find_element_by_xpath(
                "//button[@id='btn_pesquisar']/span/span").click()
        except Exception as e:
            driver.save_screenshot(cases.print_error(__name__))
            cases.logging.error(__name__ + str(e))
        try:
            sleep(2)
            res = driver.find_element_by_class_name("ui-paging-info").text
            self.assertEqual(res, "Nenhum registro para visualizar")
        except AssertionError as e:
            driver.save_screenshot(cases.print_error(__name__))
            cases.logging.error(__name__ + str(e))
            raise AssertionError(str(e))

    def test_create_message_template_email(self):
        try:
            driver = self.driver
            cases.login(driver)
            driver.get("http://{}/crm/index.php?codmodulo=509".format(cases.IP))

            driver.find_element_by_id("btn_novo").click()
            WebDriverWait(driver, 10).until(
                EC.visibility_of_element_located(
                    (By.ID, "description"))
            )
            sleep(2)
            driver.find_element_by_xpath(
                "//div[@id='lbl_id_tipo_mensagem']/div/div/a").click()
            driver.find_element_by_xpath(
                "(//a[contains(text(),'EMAIL')])[2]").click()
            driver.find_element_by_xpath(
                "//div[@id='lbl_id_layout_mailing']/div/div/a").click()
            driver.find_element_by_link_text("Campanha Fixa").click()
            driver.find_element_by_id("description").send_keys(template+"EMAIL")

            driver.find_element_by_id("sender").send_keys("Teste Remetente")
            driver.find_element_by_id("subject").send_keys("Teste template ")
            driver.find_element_by_xpath("(//span[@id='tags'])[2]").click()
            driver.find_element_by_xpath("(//span[@id='tags'])[1]").click()
            driver.find_element_by_xpath("(//span[@id='tags'])[3]").click()
            driver.find_element_by_xpath(
                "//button[@id='btn_gravar']/span/span").click()
            sleep(2)
            WebDriverWait(driver, 10).until(
                EC.invisibility_of_element_located(
                    (By.CLASS_NAME, "blockUI blockOverlay"))
            )
            driver.find_element_by_xpath(
                "//button[@id='btn_voltar']/span/span").click()
            WebDriverWait(driver, 10).until(
                EC.visibility_of_element_located(
                    (By.ID, "f_descricao"))
            )
            driver.find_element_by_id("f_descricao").send_keys(template+"EMAIL")
            driver.find_element_by_xpath(
                "//button[@id='btn_pesquisar']/span/span").click()
        except Exception as e:
            driver.save_screenshot(cases.print_error(__name__))
            cases.logging.error(__name__ + str(e))
        try:
            sleep(2)
            res = driver.find_element_by_class_name("ui-paging-info").text
            self.assertEqual(res, "Ver 1 - 1 de 1")
        except AssertionError as e:
            driver.save_screenshot(cases.print_error(__name__))
            cases.logging.error(__name__ + str(e))
            raise AssertionError(str(e))

    def test_remove_message_template_email(self):
        try:
            driver = self.driver
            cases.login(driver)
            driver.get("http://{}/crm/index.php?codmodulo=509".format(cases.IP))
            driver.find_element_by_id("f_descricao").clear()
            driver.find_element_by_id("f_descricao").send_keys(template+"EMAIL")
            driver.find_element_by_xpath(
                "//button[@id='btn_pesquisar']/span/span").click()
            sleep(1)
            el = driver.find_element_by_id("registros_listar")
            el.find_element_by_tag_name("tbody")
            el.find_elements_by_tag_name("tr")[1]
            ActionChains(driver).double_click(el).perform()
            WebDriverWait(driver, 10).until(
                EC.visibility_of_element_located(
                    (By.ID, "description"))
            )
            sleep(2)
            driver.find_element_by_xpath(
                "//div[@id='lbl_enable']/div/div/a").click()
            driver.find_element_by_xpath(
                u"(//a[contains(text(),'Não')])[2]").click()
            driver.find_element_by_xpath(
                "//button[@id='btn_gravar']/span/span").click()
            sleep(2)
            WebDriverWait(driver, 10).until(
                EC.invisibility_of_element_located(
                    (By.CLASS_NAME, "blockUI blockOverlay"))
            )
            driver.find_element_by_xpath(
                "//button[@id='btn_voltar']/span/span").click()
            WebDriverWait(driver, 10).until(
                EC.visibility_of_element_located(
                    (By.ID, "f_descricao"))
            )
            driver.find_element_by_id("f_descricao").clear()
            driver.find_element_by_id("f_descricao").send_keys(template+"EMAIL")
            driver.find_element_by_xpath(
                "//button[@id='btn_pesquisar']/span/span").click()
        except Exception as e:
            driver.save_screenshot(cases.print_error(__name__))
            cases.logging.error(__name__ + str(e))
        try:
            sleep(2)
            res = driver.find_element_by_class_name("ui-paging-info").text
            self.assertEqual(res, "Nenhum registro para visualizar")
        except AssertionError as e:
            driver.save_screenshot(cases.print_error(__name__))
            cases.logging.error(__name__ + str(e))
            raise AssertionError(str(e))

    def tearDown(self):
        self.driver.quit()


if __name__ == "__main__":
    unittest.main()
