# -*- coding: utf-8 -*-
from selenium import webdriver
import unittest
import cases
import time
from random import randint
from selenium.webdriver import ActionChains
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import Select
from selenium.webdriver.common.keys import Keys

form = "formulario" + str(randint(0, 1000))


class TestForm(unittest.TestCase):

    def setUp(self):
        self.driver = webdriver.Firefox(options=cases.options)
        self.driver.implicitly_wait(3)

    def test_create_form(self):
        driver = self.driver
        cases.login(driver)
        driver.get("http://{}/crm/index.php?codmodulo=507".format(cases.IP))
        time.sleep(2)
        WebDriverWait(driver, 10).until(
            EC.invisibility_of_element_located(
                (By.ID, "carregando_formulario_pesquisa")
            )
        )
        driver.find_element_by_id(
            "bt_cadastrar_novo_formulario").click()
        driver.find_element_by_id("nome_formulario").send_keys(form)
        driver.find_element_by_id(
            "bt_salvar_formulario").click()
        ActionChains(driver).click().perform()
        time.sleep(2)
        action = ActionChains(driver)
        action.move_by_offset(500, 500)
        action.double_click()
        action.perform()
        time.sleep(2)
        WebDriverWait(driver, 10).until(
            EC.invisibility_of_element_located(
                (By.ID, "carregando_formulario_pesquisa")
            )
        )
        driver.find_element_by_id("bt_modal_campos").click()
        driver.find_element_by_id("pergunta").send_keys("pergunta 1")
        Select(driver.find_element_by_id("tipo"))\
            .select_by_visible_text("Lista Suspensa")
        driver.find_element_by_css_selector(
            ".bootstrap-tagsinput > input:nth-child(1)")\
            .send_keys(
            "opc1", Keys.ENTER, "opc2", Keys.ENTER, "opc3", Keys.ENTER)
        driver.find_element_by_id("sequencia").send_keys("1")
        driver.find_element_by_id("habilitar").click()
        driver.find_element_by_id("bt_inserir_campo_pesquisa").click()

        #
        ActionChains(driver).click().perform()
        WebDriverWait(driver, 10).until(
            EC.invisibility_of_element_located((By.ID, "modal-backdrop fade"))
        )
        driver.find_element_by_id("bt_modal_campos").click()
        driver.find_element_by_id("pergunta").send_keys("pergunta 2")
        Select(driver.find_element_by_id("tipo"))\
            .select_by_visible_text("Caixa de Seleção")
        driver.find_element_by_css_selector(
            ".bootstrap-tagsinput > input:nth-child(1)")\
            .send_keys(
            "opc1", Keys.ENTER, "opc2", Keys.ENTER, "opc3", Keys.ENTER)
        driver.find_element_by_id("sequencia").send_keys("2")
        driver.find_element_by_id("habilitar").click()
        driver.find_element_by_id("bt_inserir_campo_pesquisa").click()
        time.sleep(1)
        driver.find_element_by_id("bt_salvar_questoes_pesquisa").click()
        time.sleep(1)
        res = driver.find_element_by_id("swal2-content").text
        try:
            self.assertEqual(res, "Perguntas cadastradas com sucesso!")
        except AssertionError as e:
            driver.save_screenshot(cases.print_error(__name__))
            cases.logging.error(
                "Ocorreu algum erro ao criar uma pausa: " + str(e))
            raise AssertionError(
                "Ocorreu algum erro ao criar uma pausa:" + str(e))

    def tearDown(self):
        self.driver.quit()


if __name__ == "__main__":
    unittest.main()
