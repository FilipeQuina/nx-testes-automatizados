# -*- coding: utf-8 -*-
from selenium import webdriver
import unittest
import time
import cases
from random import randint
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

perfil = "perfil" + str(randint(0, 1000)).zfill(4)


class TestPerfilAcesso(unittest.TestCase):

    def setUp(self):
        self.driver = webdriver.Firefox(options=cases.options)
        self.driver.implicitly_wait(3)

    def test_create_access_perfil(self):
        try:
            driver = self.driver
            cases.login(driver)
            driver.get("http://{}/crm/index.php?codmodulo=428".format(cases.IP))

            driver.find_element_by_id("btn_novo").click()
            driver.find_element_by_id("descricao").send_keys(perfil)
            driver.find_element_by_id("btn_gravar").click()
            time.sleep(2)
            WebDriverWait(driver, 10).until(
                EC.invisibility_of_element_located(
                    (By.CLASS_NAME, "blockUI blockOverlay"))
            )
            driver.find_element_by_id("f_descricao").send_keys(perfil)
            driver.find_element_by_id("btn_buscar").click()
            WebDriverWait(driver, 10).until(
                EC.text_to_be_present_in_element(
                    (By.CLASS_NAME, "ui-paging-info"), "Ver 1 - 1 de 1")
            )
            perfil_assert = driver.find_element_by_id("grid")\
                .find_element_by_tag_name("tbody")\
                .find_elements_by_tag_name("tr")[1]\
                .find_elements_by_tag_name("td")[2]\
                .get_attribute("innerHTML")

        except Exception as e:
            driver.save_screenshot(cases.print_error(__name__))
            cases.logging.error(__name__ + str(e))

        try:
            self.assertEqual(perfil, perfil_assert)
        except AssertionError as e:
            driver.save_screenshot(cases.print_error(__name__))
            cases.logging.error(__name__ + str(e))
            raise AssertionError(str(e))

    def tearDown(self):
        self.driver.quit()


if __name__ == "__main__":
    unittest.main()
