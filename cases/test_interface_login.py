# -*- coding: utf-8 -*-
from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException
import unittest
import cases


class TestLogin(unittest.TestCase):

    def setUp(self):
        self.driver = webdriver.Firefox(options=cases.options)
        self.driver.implicitly_wait(10)

    def test_login_wrong(self):
        driver = self.driver
        driver.get("http://{}/crm/login.php".format(cases.IP))
        driver.find_element_by_id("l_login").send_keys("suporte")
        driver.find_element_by_id("l_senha").send_keys("PASS")
        driver.find_element_by_css_selector(".btn,.btn-send").click()
        try:
            driver.find_element_by_css_selector("span.title:nth-child(1)")
        except (NoSuchElementException, AssertionError):
            cases.logging.error(
                "Login passou com uma senha errada, verificar segurança!")
            raise NoSuchElementException(
                "Login passou com uma senha errada, verificar segurança!")

    def test_login_correct(self):
        driver = self.driver
        driver.get("http://{}/crm/login.php".format(cases.IP))
        driver.find_element_by_id("l_login").send_keys("suporte")
        driver.find_element_by_id("l_senha").send_keys(cases.PASS)
        driver.find_element_by_css_selector(".btn,.btn-send").click()
        try:
            self.assertEqual(driver.find_element_by_id(
                "nomemodulo").text, '| Home')
        except (AssertionError, NoSuchElementException) as e:
            driver.save_screenshot(cases.print_error(__name__))
            cases.logging.error(__name__ + str(e))
            raise AssertionError(str(e))

    def tearDown(self):
        self.driver.quit()


if __name__ == "__main__":
    unittest.main()
