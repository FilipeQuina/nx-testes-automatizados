#!/bin/bash

BUILD=$RANDOM
echo $BUILD > build.txt
python -m unittest  discover -s cases -p "test_*.py" 2> retorno$BUILD.txt
echo "ok"
RESULT=$(tail -1 retorno$BUILD.txt)
VAR1="OK"
if [ "$RESULT" = "$VAR1" ]; then 
    echo "funcionou"
else
    tar -czvf report.tar.gz $BUILD
    mpack -s "Erros ao validar os testes automatizados" -d layout.txt report.tar.gz filipe.pacheco@smartnx.io
    rm report.tar.gz
fi
rm -rf $BUILD/
rm retorno$BUILD.txt