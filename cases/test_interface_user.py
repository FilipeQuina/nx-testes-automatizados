# -*- coding: utf-8 -*-
from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import ElementClickInterceptedException
import unittest
import time
import cases
from random import randint
from selenium.webdriver import ActionChains
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

user_name = "filipe" + str(randint(0, 1000)).zfill(4)


class TestUser(unittest.TestCase):

    def setUp(self):
        self.driver = webdriver.Firefox(options=cases.options)
        self.driver.implicitly_wait(3)

    def test_create_user(self):
        try:
            driver = self.driver
            cases.login(driver)
            driver.get("http://{}/crm/index.php?codmodulo=7".format(cases.IP))
            driver.find_element_by_xpath(
                "//button[@id='btn_novo']/span/span").click()
            driver.find_element_by_xpath(
                "//form[@id='frmModUsuarios']/fieldset[2]/dl/div[2]/dd/div/div/a").click()
            driver.find_element_by_link_text("1 - Operação").click()
            driver.find_element_by_xpath(
                "//form[@id='frmModUsuarios']/fieldset[2]/dl/div[3]/dd/div/div/a").click()
            driver.find_element_by_link_text("Padrão").click()
            driver.find_element_by_id("nome").send_keys("Filipe Pacheco Teste")
            driver.find_element_by_id("login").send_keys(user_name)
            driver.find_element_by_id("senha").send_keys("10203040")
            driver.find_element_by_xpath(
                "//form[@id='frmModUsuarios']/fieldset[2]").click()
            driver.find_element_by_xpath(
                "//form[@id='frmModUsuarios']/fieldset[2]/dl[4]/div[3]/dd/div/div/span").click()
            driver.find_element_by_xpath(
                "//form[@id='frmModUsuarios']/fieldset[2]").click()
            driver.find_element_by_xpath(
                "//form[@id='frmModUsuarios']/fieldset[2]/dl[4]/div[6]/dd/div/div/span").click()
            driver.find_element_by_xpath(
                "(//a[contains(text(),'Sim')])[2]").click()
            driver.find_element_by_xpath("//dt[@id='boxExcel']/span/a").click()
            driver.find_element_by_xpath(
                "//dt[@id='boxvisualizartodosniveis']/span/a").click()
            driver.find_element_by_xpath(
                "//button[@id='btn_gravar']/span/span").click()
            time.sleep(2)
            WebDriverWait(driver, 10).until(
                EC.invisibility_of_element(
                    (By.CLASS_NAME, "blockUI blockOverlay"))
            )
            driver.find_element_by_id("pesq_buscar").send_keys(user_name)
            driver.find_element_by_id("btn_pesquisar").click()
            time.sleep(2)
            WebDriverWait(driver, 10).until(
                EC.text_to_be_present_in_element(
                    (By.CLASS_NAME, "ui-paging-info"), "Ver 1 - 1 de 1")
            )
            user_name_assert = driver.find_elements_by_tag_name("tbody")[3]\
                .find_elements_by_tag_name("tr")[1]\
                .find_elements_by_tag_name("td")[5]\
                .get_attribute("innerHTML")
        except Exception as e:
            driver.save_screenshot(cases.print_error(__name__))
            cases.logging.error(__name__ + str(e))
        try:
            self.assertEqual(user_name, user_name_assert)
        except AssertionError as e:
            driver.save_screenshot(cases.print_error(__name__))
            cases.logging.error(__name__ + str(e))
            raise AssertionError(str(e))
        
    '''
    def test_delete_user(self):
        try:
            driver = self.driver
            cases.login(driver)
            driver.get("http://{}/crm/index.php?codmodulo=7".format(cases.IP))
            driver.find_element_by_id("pesq_buscar").send_keys("teste")
            driver.find_element_by_id("btn_pesquisar").click()
            time.sleep(1)
            element = driver.find_elements_by_tag_name("tbody")[3]\
                .find_elements_by_tag_name("tr")[1]\
                .find_elements_by_tag_name("td")[5]
            action = ActionChains(self.driver)
            action.double_click(element)
            action.perform()
            WebDriverWait(driver, 10).until(
                EC.invisibility_of_element(
                    (By.ID, "btn_deletar"))
            )
            driver.find_element_by_id("btn_deletar").click()
            data_picker = driver.find_element_by_id("ui-datepicker-div")
            data_picker.find_elements_by_tag_name("tr")[1]\
                .find_elements_by_tag_name("td")[3]
            action = ActionChains(self.driver)
            action.double_click(data_picker)
            action.perform()
        except Exception as e:
            driver.save_screenshot(cases.print_error(__name__))
            cases.logging.error(__name__ + str(e))
        try:
            driver.find_element_by_css_selector("button.in").click()
        except ElementClickInterceptedException as e:
            cases.logging.error(
                "Houve um erro ao deletar o usuário:" + str(e))
            raise NoSuchElementException(str(e))
    '''

    def tearDown(self):
        self.driver.quit()


if __name__ == "__main__":
    unittest.main()
