# -*- coding: utf-8 -*-
from selenium import webdriver
import cases
import unittest
import time
from random import randint
from selenium.webdriver import ActionChains
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.keys import Keys
condtemp = "condtemp" + str(randint(0, 99999)).zfill(5)


class TestTimeConditions(unittest.TestCase):
    def setUp(self):
        self.driver = webdriver.Firefox(options=cases.options)
        self.driver.implicitly_wait(3)

    def test_create_time_conditions(self):
        try:
            driver = self.driver
            cases.login(driver)
            driver.get("http://{}/crm/index.php?codmodulo=395"
                       .format(cases.IP))
            driver.find_element_by_xpath(
                "//button[@id='btn_novo']/span/span").click()
            driver.find_element_by_id("condicao_tempo").send_keys(condtemp)
            driver.find_element_by_xpath(
                "//div[@id='lbl_opcao_feriados']/dd/div/div/span").click()
            driver.find_element_by_link_text("Excluir").click()
            driver.find_element_by_xpath(
                "//div[@id='lbl_destino_tipo']/dd/div/div/span").click()
            driver.find_element_by_link_text("URA").click()
            driver.find_element_by_xpath(
                "//div[@id='lbl_destino_tipo']/dd/div/div/span").click()
            driver.find_element_by_link_text("Ramal").click()
            driver.find_element_by_id("destino_valor").send_keys("9999")
            driver.find_element_by_xpath(
                "//div[@id='lbl_destino_tipo_fh']/dd/div/div/span").click()
            time.sleep(2)
            driver.find_element_by_xpath(
                "(//a[contains(text(),'URA')])[6]").click()
            driver.find_element_by_xpath(
                "//div[@id='lbl_id_ura_in_fh']/dd/div/div/span").click()
            driver.find_element_by_xpath(
                "(//a[contains(text(),'ura_principal')])[3]").click()
            driver.find_element_by_xpath(
                "//button[@id='btn_gravar']/span/span").click()
            time.sleep(1)
            WebDriverWait(driver, 10).until(
                EC.invisibility_of_element_located(
                    (By.CLASS_NAME, "blockUI blockOverlay"))
            )
            driver.find_element_by_xpath(
                "//button[@id='btn_canceledit']/span/span").click()
            driver.find_element_by_id("f_condicao_tempo").send_keys(condtemp)
            driver.find_element_by_xpath(
                "//button[@id='btn_pesquisar']/span/span").click()
            time.sleep(2)
            condtemp_assert = driver.find_element_by_id("registros_listar")\
                .find_element_by_tag_name("tbody")\
                .find_elements_by_tag_name("tr")[1]\
                .find_elements_by_tag_name("td")[3]\
                .get_attribute("innerHTML")
        except Exception as e:
            driver.save_screenshot(cases.print_error(__name__))
            cases.logging.error(__name__ + str(e))
        try:
            self.assertEqual(condtemp, condtemp_assert)
        except AssertionError as e:
            driver.save_screenshot(cases.print_error(__name__))
            cases.logging.error(__name__ + str(e))
            raise AssertionError(str(e))

    def test_remove_time_conditions(self):
        try:
            driver = self.driver
            cases.login(driver)
            driver.get("http://{}/crm/index.php?codmodulo=395"
                       .format(cases.IP))
            driver.find_element_by_id("f_condicao_tempo").send_keys(condtemp)
            driver.find_element_by_xpath(
                "//button[@id='btn_pesquisar']/span/span").click()
            time.sleep(2)
            condtemp_assert = driver.find_element_by_id("registros_listar")\
                .find_element_by_tag_name("tbody")\
                .find_elements_by_tag_name("tr")[1]
            WebDriverWait(driver, 10).until(
                EC.text_to_be_present_in_element(
                    (By.CLASS_NAME, "ui-paging-info"),"Ver 1 - 1 de 1"
                )
            )
            action = ActionChains(driver)
            action.double_click(condtemp_assert)
            action.perform()
            WebDriverWait(driver, 10).until(
                EC.visibility_of_element_located(
                    (By.ID, "condicao_tempo")
                )
            )
            driver.find_element_by_id("btn_deletar").click()
            time.sleep(1)
            action = ActionChains(driver)
            action.key_down(Keys.ENTER)
            action.perform()
            time.sleep(1)

        except Exception as e:
            driver.save_screenshot(cases.print_error(__name__))
            cases.logging.error(__name__ + str(e))
        try:
            res = driver.find_element_by_class_name("ui-paging-info").text
            self.assertEqual(res, "Nenhum registro para visualizar")
        except AssertionError as e:
            driver.save_screenshot(cases.print_error(__name__))
            cases.logging.error(__name__ + str(e))
            raise AssertionError(str(e))

    def tearDown(self):
        self.driver.quit()


if __name__ == "__main__":
    unittest.main()
