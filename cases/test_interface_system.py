# -*- coding: utf-8 -*-
from selenium import webdriver
import unittest
import time
import cases
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC


class TestSystem(unittest.TestCase):

    def setUp(self):
        self.driver = webdriver.Firefox(options=cases.options)
        self.driver.implicitly_wait(3)

    def test_conf(self):
        try:
            driver = self.driver
            cases.login(driver)
            driver.get("http://{}/crm/index.php?codmodulo=31".format(cases.IP))
            driver.find_element_by_id("qtderobos_discador").click()
            time.sleep(1)
            driver.find_element_by_css_selector(
                ".swal2-cancel.swal2-styled").click()
            driver.find_element_by_id("qtdelicencas_robo").click()
            driver.find_element_by_css_selector(
                ".swal2-cancel.swal2-styled").click()
            driver.save_screenshot(__name__+"A.png")
            time.sleep(1)
            driver.find_element_by_id("btn_gravar").click()
            time.sleep(2)
            WebDriverWait(driver, 10).until(
                EC.invisibility_of_element_located(
                    (By.CLASS_NAME, "blockUI blockOverlay"))
            )
            driver.find_element_by_id("menu_sair").click()
            WebDriverWait(driver, 10).until(
                EC.visibility_of_element_located(
                    (By.ID, "l_login"))
            )
            cases.login(driver)
            driver.get("http://{}/crm/index.php?codmodulo=31"
                       .format(cases.IP))
            WebDriverWait(driver, 10).until(
                EC.visibility_of_element_located(
                    (By.ID, "lbl_qtderobos_discador"))
            )
            driver.save_screenshot(__name__+"B.png")
        except Exception as e:
            driver.save_screenshot(cases.print_error(__name__))
            cases.logging.error(__name__ + str(e))
        try:
            res = cases.mse(self, __name__+"A.png", __name__+"B.png")
            self.assertAlmostEqual(0.0, float(res), delta=5.0)
        except AssertionError as e:
            driver.save_screenshot(cases.print_error(__name__))
            cases.logging.error(__name__ + str(e))
            raise AssertionError(str(e))

    def tearDown(self):
        self.driver.quit()


if __name__ == "__main__":
    unittest.main()
