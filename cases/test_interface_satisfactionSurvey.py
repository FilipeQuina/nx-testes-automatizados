# -*- coding: utf-8 -*-
from selenium import webdriver
import cases
import unittest
from time import sleep
from random import randint
from selenium.webdriver import ActionChains
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.keys import Keys
pesq = "pesq"+str(randint(0, 99999)).zfill(5)


class TestSatisfactionSurvey(unittest.TestCase):
    def setUp(self):
        self.driver = webdriver.Firefox(options=cases.options)
        self.driver.implicitly_wait(3)

    def test_create_satisfaction_survey(self):
        try:
            driver = self.driver
            cases.login(driver)
            driver.get("http://{}/crm/index.php?codmodulo=443"
                       .format(cases.IP))
            driver.find_element_by_xpath("//button[@id='btn_novo']/span/span").click()
            driver.find_element_by_id("pesquisa").send_keys(pesq)
            driver.find_element_by_id("lookup_id_audio").click()
            driver.find_element_by_id("btn_id_audio_u_2").click()
            driver.find_element_by_xpath("//dd[@id='sctLdestino_tipo']/div/div/span").click()
            driver.find_element_by_link_text(u"Pesquisa/Avaliação").click()
            driver.find_element_by_xpath("//div[@id='lbl_d_id_pesquisa']/dd/div/div/a").click()
            driver.find_element_by_xpath("//dd[@id='sctLdestino_tipo']/div/div/a").click()
            driver.find_element_by_link_text("Terminar Chamada").click()
            driver.find_element_by_xpath("//button[@id='btn_gravar']/span/span").click()
            sleep(2)
            WebDriverWait(driver, 10).until(
                EC.invisibility_of_element_located(
                    (By.CLASS_NAME, "blockUI blockOverlay"))
            )
            driver.find_element_by_id("f_pesquisa").send_keys(pesq)
            driver.find_element_by_xpath("//button[@id='btn_buscar']/span/span").click()
        except Exception as e:
            driver.save_screenshot(cases.print_error(__name__))
            cases.logging.error(__name__ + str(e))
        try:
            sleep(2)
            self.assertEqual("Ver 1 - 1 de 1",
                             driver.find_element_by_class_name(
                                "ui-paging-info").text)
        except AssertionError as e:
            driver.save_screenshot(cases.print_error(__name__))
            cases.logging.error(__name__ + str(e))
            raise AssertionError(str(e))

    def test_remove_satisfaction_survey(self):
        try:
            driver = self.driver
            cases.login(driver)
            driver.get("http://{}/crm/index.php?codmodulo=443"
                       .format(cases.IP))
            driver.find_element_by_id("f_pesquisa").send_keys(pesq)
            driver.find_element_by_id("btn_buscar").click()
            sleep(1)
            el = driver.find_element_by_id("grid")\
                .find_element_by_tag_name("tbody")\
                .find_elements_by_tag_name("tr")[1]
            action = ActionChains(driver)
            action.double_click(el)
            action.perform()
            WebDriverWait(driver, 10).until(
                EC.visibility_of_element_located(
                    (By.ID, "pesquisa")
                )
            )
            driver.find_element_by_id("btn_deletar").click()
            sleep(1)
            action = ActionChains(driver)
            action.key_down(Keys.ENTER)
            action.perform()
            sleep(1)
        except Exception as e:
            driver.save_screenshot(cases.print_error(__name__))
            cases.logging.error(__name__ + str(e))
        try:
            res = driver.find_element_by_class_name("ui-paging-info").text
            self.assertEqual(res, "Nenhum registro para visualizar")
        except AssertionError as e:
            driver.save_screenshot(cases.print_error(__name__))
            cases.logging.error(__name__ + str(e))
            raise AssertionError(str(e))

    def tearDown(self):
        self.driver.quit()


if __name__ == "__main__":
    unittest.main()
