# -*- coding: utf-8 -*-
from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException
import unittest
import time
import cases
from random import randint
from selenium.webdriver import ActionChains
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

user_name = "filipe" + str(randint(0, 1000))


class TestHideMenu(unittest.TestCase):

    def setUp(self):
        self.driver = webdriver.Firefox(options=cases.options)
        self.driver.implicitly_wait(3)

    def test_hide_menu(self):
        try:
            driver = self.driver
            cases.login(driver)
            driver.get("http://{}/crm/index.php?codmodulo=5".format(cases.IP))

            driver.find_element_by_id(
                "f_descricao").send_keys("Transmitir pedidos")
            driver.find_element_by_id("btn_buscar").click()
            WebDriverWait(driver, 10).until(
                EC.text_to_be_present_in_element(
                    (By.CLASS_NAME, "ui-paging-info"), "Ver 1 - 1 de 1")
            )
            el = driver.find_element_by_id("grid")\
                .find_element_by_tag_name("tbody")\
                .find_elements_by_tag_name("tr")[1]

            action = ActionChains(driver)
            action.double_click(el)
            action.perform()
            WebDriverWait(driver, 10).until(
                EC.visibility_of_element_located(
                    (By.ID, "descricao"))
            )
            show_class = driver.find_element_by_id("boxhabilitar")\
                .find_element_by_tag_name("span")\
                .find_element_by_tag_name("a")\
                .get_attribute("class")

            if show_class == "jqTransformCheckbox jqTransformChecked":
                driver.find_element_by_id("boxhabilitar")\
                    .find_element_by_tag_name("span")\
                    .find_element_by_tag_name("a")\
                    .click()

            driver.find_element_by_id("btn_gravar").click()
            time.sleep(1)
            WebDriverWait(driver, 10).until(
                EC.invisibility_of_element_located(
                    (By.CLASS_NAME, "blockUI blockOverlay"))
            )
            driver.find_element_by_id("menu_sair").click()
            driver.delete_all_cookies()
            cases.login(driver)
            driver.get("http://{}/crm/index.php?codmodulo=5".format(cases.IP))
            driver.find_element_by_id("menu_administracao").click()
        except Exception as e:
            driver.save_screenshot(cases.print_error(__name__))
            cases.logging.error(__name__ + str(e))
        try:
            menu_assert = driver.find_element_by_css_selector(".fg-menu")\
                .find_element_by_link_text("Transmitir pedidos")\
                .text
        except NoSuchElementException:
            menu_assert = "--MENU NAO ENCONTRADO--"

        try:
            self.assertEqual("--MENU NAO ENCONTRADO--", menu_assert)
        except AssertionError as e:
            driver.save_screenshot(cases.print_error(__name__))
            cases.logging.error(__name__ + str(e))
            raise AssertionError(str(e))

    def test_show_menu(self):
        try:
            driver = self.driver
            cases.login(driver)
            driver.get("http://{}/crm/index.php?codmodulo=5".format(cases.IP))
            driver.find_element_by_id(
                "f_descricao").send_keys("Transmitir pedidos")
            driver.find_element_by_id("btn_buscar").click()
            WebDriverWait(driver, 10).until(
                EC.text_to_be_present_in_element(
                    (By.CLASS_NAME, "ui-paging-info"), "Ver 1 - 1 de 1")
            )
            el = driver.find_element_by_id("grid")\
                .find_element_by_tag_name("tbody")\
                .find_elements_by_tag_name("tr")[1]

            action = ActionChains(driver)
            action.double_click(el)
            action.perform()
            WebDriverWait(driver, 10).until(
                EC.visibility_of_element_located(
                    (By.ID, "descricao"))
            )
            show_class = driver.find_element_by_id("boxhabilitar")\
                .find_element_by_tag_name("span")\
                .find_element_by_tag_name("a")\
                .get_attribute("class")

            if show_class != "jqTransformCheckbox jqTransformChecked":
                driver.find_element_by_id("boxhabilitar")\
                    .find_element_by_tag_name("span")\
                    .find_element_by_tag_name("a")\
                    .click()

            driver.find_element_by_id("btn_gravar").click()
            time.sleep(1)
            WebDriverWait(driver, 10).until(
                EC.invisibility_of_element_located(
                    (By.CLASS_NAME, "blockUI blockOverlay"))
            )
            driver.find_element_by_id("menu_sair").click()
            driver.delete_all_cookies()
            cases.login(driver)
            driver.get("http://{}/crm/index.php?codmodulo=5".format(cases.IP))
            driver.find_element_by_id("menu_administracao").click()
            menu_assert = driver.find_element_by_css_selector(".fg-menu")\
                .find_element_by_link_text("Transmitir pedidos")\
                .text
        except Exception as e:
            driver.save_screenshot(cases.print_error(__name__))
            cases.logging.error(__name__ + str(e))

        try:
            self.assertEqual("Transmitir pedidos", menu_assert)
        except AssertionError as e:
            driver.save_screenshot(cases.print_error(__name__))
            cases.logging.error(__name__ + str(e))
            raise AssertionError(str(e))

    def tearDown(self):
        self.driver.quit()


if __name__ == "__main__":
    unittest.main()
