from selenium import webdriver
import unittest
from time import sleep
import cases
from random import randint
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver import ActionChains
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.keys import Keys
rand_number_room = randint(1, 4000)
rand_senha = randint(9999, 999999)
rand_admin_senha = randint(9999, 999999)


class TestSalaConferencia(unittest.TestCase):
    def setUp(self):
        self.driver = webdriver.Firefox(options=cases.options)
        self.driver.implicitly_wait(3)

    def test_create_new_room(self):
        try:
            driver = self.driver
            cases.login(driver)
            driver.get("http://{}/crm/index.php?codmodulo=384"
                       .format(cases.IP))
            driver.find_element_by_xpath(
                '//*[@id="btn_novo"]').click()
            driver.find_element_by_id('meetme').clear()
            driver.find_element_by_id('meetme').send_keys(rand_number_room)
            driver.find_element_by_id('senha').send_keys(rand_senha)
            driver.find_element_by_id(
                'senha_admin').send_keys(rand_admin_senha)
            sleep(1)
            driver.find_element_by_xpath(
                '//*[@id="btn_gravar"]').click()
            sleep(2)
            room_conf_assert = driver.find_element_by_id("registros_listar")\
                .find_element_by_tag_name("tbody")\
                .find_elements_by_tag_name("tr")[-1]\
                .find_elements_by_tag_name("td")[2]\
                .get_attribute("innerHTML")

        except Exception as e:
            driver.save_screenshot(cases.print_error(__name__))
            cases.logging.error(__name__ + str(e))
        try:
            self.assertEqual(int(room_conf_assert), rand_number_room)
        except AssertionError as e:
            driver.save_screenshot(cases.print_error(__name__))
            cases.logging.error(__name__ + str(e))
            raise AssertionError(str(e))

    def test_delete_room(self):
        try:
            driver = self.driver
            cases.login(driver)
            driver.get("http://{}/crm/index.php?codmodulo=384"
                       .format(cases.IP))
            WebDriverWait(driver, 10).until(
                EC.visibility_of_element_located(
                    (By.CLASS_NAME, "ui-paging-info"))
            )
            qtd_inicial = driver.find_element_by_class_name(
                'ui-paging-info').text[-1]

            grid_element_assert = driver.find_element_by_id("registros_listar")\
                .find_element_by_tag_name("tbody")\
                .find_elements_by_tag_name("tr")[-1]
            action = ActionChains(driver)
            action.double_click(grid_element_assert)
            action.perform()
            sleep(2)
            driver.find_element_by_xpath(
                '//*[@id="btn_deletar"]').click()
            sleep(1)
            action = ActionChains(driver)
            action.key_down(Keys.ENTER)
            action.perform()
            sleep(1)
        except Exception as e:
            driver.save_screenshot(cases.print_error(__name__))
            cases.logging.error(__name__ + str(e))
        try:
            WebDriverWait(driver, 10).until(
                EC.visibility_of_element_located(
                    (By.CLASS_NAME, "ui-paging-info"))
            )
            res = driver.find_element_by_class_name("ui-paging-info").text[-1]
            if res == 'r':
                self.assertEqual(
                    'Nenhum registro para visualizar',
                    driver.find_element_by_class_name("ui-paging-info").text)
            else:
                res = int(res)+1
                self.assertEqual(qtd_inicial, res)
        except AssertionError as e:
            driver.save_screenshot(cases.print_error(__name__))
            cases.logging.error(__name__ + str(e))
            raise AssertionError(str(e))

    def tearDown(self):
        self.driver.quit()


if __name__ == "__main__":
    unittest.main()
