# -*- coding: utf-8 -*-
from selenium import webdriver
import cases
import unittest
from time import sleep
from random import randint
from selenium.webdriver import ActionChains
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
group = str(randint(0, 99999)).zfill(5)


class TestCallGroup(unittest.TestCase):
    def setUp(self):
        self.driver = webdriver.Firefox(options=cases.options)
        self.driver.implicitly_wait(3)

    def test_create_call_group(self):
        try:
            driver = self.driver
            cases.login(driver)
            driver.get("http://{}/crm/index.php?codmodulo=342"
                       .format(cases.IP))
            driver.find_element_by_xpath(
                "//button[@id='btn_novo']/span/span").click()
            driver.find_element_by_id("grupo_chamada").send_keys(group)
            driver.find_element_by_xpath(
                "//div[@id='lbl_estrategia']/dd/div/div/a").click()
            driver.find_element_by_xpath(
                "(//a[contains(text(),'Chamar Todos')])[2]").click()
            driver.find_element_by_id("descricao").send_keys(u"opção 5 ura")
            driver.find_element_by_id("tempo_dial").send_keys("60")
            driver.find_element_by_xpath(
                "//button[@id='btn_gravar']/span/span/img").click()
            sleep(2)
            WebDriverWait(driver, 10).until(
                EC.invisibility_of_element(
                    (By.CLASS_NAME, "blockUI blockOverlay")
                )
            )
            driver.find_element_by_id("f_grupo_chamada").send_keys(group)
            driver.find_element_by_xpath(
                "//button[@id='btn_pesquisar']/span/span").click()

            driver.find_element_by_id("f_grupo_chamada").click()

            driver.find_element_by_id("f_grupo_chamada").send_keys("1")
            driver.find_element_by_xpath(
                "//div[@id='gview_registros_listar']/div[3]").click()
            driver.find_element_by_id("f_grupo_chamada").send_keys(group)
            driver.find_element_by_xpath(
                "//button[@id='btn_pesquisar']/span/span/img").click()

        except Exception as e:
            driver.save_screenshot(cases.print_error(__name__))
            cases.logging.error(__name__ + str(e))
        try:
            self.assertEqual("Ver 1 - 1 de 1",
                             driver.find_element_by_xpath(
                                "//td[@id='registros_pager_right']/div").text)
        except AssertionError as e:
            driver.save_screenshot(cases.print_error(__name__))
            cases.logging.error(__name__ + str(e))
            raise AssertionError(str(e))

    def test_remove_call_group(self):
        try:
            driver = self.driver
            cases.login(driver)
            driver.get("http://{}/crm/index.php?codmodulo=342"
                       .format(cases.IP))

            driver.find_element_by_id("f_grupo_chamada").send_keys(group)
            driver.find_element_by_id("btn_pesquisar").click()
            sleep(1)
            el = driver.find_element_by_id("registros_listar")\
                .find_element_by_tag_name("tbody")\
                .find_elements_by_tag_name("tr")[1]
            action = ActionChains(driver)
            action.double_click(el)
            action.perform()
            WebDriverWait(driver, 10).until(
                EC.visibility_of_element_located(
                    (By.ID, "grupo_chamada")
                )
            )
            driver.find_element_by_xpath(
                "//dt[@id='boxhabilitar']/span/a").click()
            driver.find_element_by_xpath(
                "//button[@id='btn_gravar']/span/span/img").click()
            sleep(2)
            WebDriverWait(driver, 10).until(
                EC.invisibility_of_element(
                    (By.CLASS_NAME, "blockUI blockOverlay")
                )
            )
            driver.find_element_by_xpath(
                "//button[@id='btn_pesquisar']/span/span").click()
            sleep(2)
            grid_el_assert = driver.find_element_by_id("registros_listar")\
                .find_element_by_tag_name("tbody")\
                .find_elements_by_tag_name("tr")[1]\
                .find_elements_by_tag_name("td")[8]\
                .get_attribute("innerHTML")

        except Exception as e:
            driver.save_screenshot(cases.print_error(__name__))
            cases.logging.error(__name__ + str(e))
        try:

            self.assertEqual(u"Não", grid_el_assert)
        except AssertionError as e:
            driver.save_screenshot(cases.print_error(__name__))
            cases.logging.error(__name__ + str(e))
            raise AssertionError(str(e))

    def tearDown(self):
        self.driver.quit()


if __name__ == "__main__":
    unittest.main()
