from selenium import webdriver
import unittest
from time import sleep
import cases
from random import randint
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver import ActionChains
from selenium.webdriver.support import expected_conditions as EC
randAlias = randint(1, 999)
alias = 'TESTE ALIAS {}'.format(randAlias)


class TestAlias(unittest.TestCase):
    def setUp(self):
        self.driver = webdriver.Firefox(options=cases.options)
        self.driver.implicitly_wait(3)

    def test_create_alias(self):
        try:
            driver = self.driver
            cases.login(driver)
            driver.get("http://{}/crm/index.php?codmodulo=400"
                       .format(cases.IP))
            driver.save_screenshot(__name__+'C.png')
            driver.find_element_by_xpath(
                '//*[@id="btn_novo"]/span/span').click()
            driver.find_element_by_xpath('//*[@id="alias"]').send_keys(alias)
            driver.find_element_by_xpath(
                '//*[@id="lbl_destino"]/dd/div/div/a').click()
            driver.find_element_by_xpath(
                '//*[@id="lbl_destino"]/dd/div/ul/li[9]/a').click()
            sleep(1)
            driver.find_element_by_xpath(
                '//*[@id="lbl_id_ura_in"]/dd/div/div/a').click()
            driver.find_element_by_xpath(
                '/html/body/div[3]/div[2]/div[1]/div[2]/form/fieldset[2]/dl/div[8]/dd/div/ul/li[2]/a').click()
            driver.find_element_by_xpath(
                '//*[@id="boxhabilitar"]/span/a').click()
            driver.find_element_by_xpath(
                '//*[@id="boxhabilitar"]/span/a').click()
            sleep(2)
            driver.save_screenshot(__name__+'A.png')
            driver.find_element_by_xpath('//*[@id="btn_gravar"]').click()
            sleep(4)
            grid_element_assert = driver.find_element_by_id("registros_listar")\
                .find_element_by_tag_name("tbody")\
                .find_elements_by_tag_name("tr")[-1]\
                .find_elements_by_tag_name("td")[-1]
            action = ActionChains(self.driver)
            action.double_click(grid_element_assert)
            action.perform()
            WebDriverWait(driver, 10).until(
                EC.visibility_of_element_located(
                    (By.XPATH, '//*[@id="btn_canceledit"]')
                )
            )
            # Salvando o print B
            driver.save_screenshot(__name__ + 'B.png')
            driver.find_element_by_xpath('//*[@id="btn_canceledit"]').click()
        except Exception as e:
            driver.save_screenshot(cases.print_error(__name__))
            cases.logging.error(__name__ + str(e))
        try:
            res = cases.mse(self, 'test_interface_aliasA.png',
                            'test_interface_aliasB.png')
            self.assertAlmostEqual(0.0, float(res), delta=50.00)
        except AssertionError as e:
            driver.save_screenshot(cases.print_error(__name__))
            cases.logging.error(__name__ + str(e))
            raise AssertionError(str(e))

    def test_delete_alias(self):
        try:
            driver = self.driver
            cases.login(driver)
            driver.get("http://{}/crm/index.php?codmodulo=400"
                       .format(cases.IP))
            driver.find_element_by_id("f_destino").send_keys(alias)
            driver.find_element_by_id("btn_pesquisar").click()
            sleep(2)
            grid_element_assert = driver.find_element_by_id("registros_listar")\
                .find_element_by_tag_name("tbody")\
                .find_elements_by_tag_name("tr")[1]
            action = ActionChains(driver)
            action.double_click(grid_element_assert)
            action.perform()
            sleep(1)
            WebDriverWait(driver, 10).until(
                EC.visibility_of_element_located(
                    (By.ID, "btn_deletar")
                )
            )
            driver.find_element_by_xpath('//*[@id="btn_deletar"]').click()
            sleep(1)
            WebDriverWait(driver, 10).until(
                EC.visibility_of_element_located(
                    (By.CSS_SELECTOR, ".ui-button.ui-widget.ui-state-default.ui-corner-all.ui-button-text-only")
                )
            )
            driver.find_element_by_css_selector('.ui-button.ui-widget.ui-state-default.ui-corner-all.ui-button-text-only').click()
            sleep(4)
        except Exception as e:
            driver.save_screenshot(cases.print_error(__name__))
            cases.logging.error(__name__ + str(e))
        try:
            driver.find_element_by_xpath('//*[@id="f_destino"]').click()
            driver.find_element_by_xpath(
                '//*[@id="f_destino"]').clear()
            driver.find_element_by_xpath(
                '//*[@id="f_destino"]').send_keys(alias)
            driver.find_element_by_xpath('//*[@id="btn_pesquisar"]').click()
            res = driver.find_element_by_class_name("ui-paging-info").text
            self.assertEqual(res, "Nenhum registro para visualizar")

        except AssertionError as e:
            driver.save_screenshot(cases.print_error(__name__))
            cases.logging.error(__name__ + str(e))
            raise AssertionError(str(e))

    def tearDown(self):
        self.driver.quit()


if __name__ == "__main__":
    unittest.main()
