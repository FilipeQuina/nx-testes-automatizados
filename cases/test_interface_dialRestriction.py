# -*- coding: utf-8 -*-
# -*- coding: utf-8 -*-
from selenium import webdriver
import unittest
import cases
import time
from random import randint
from selenium.webdriver import ActionChains
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.keys import Keys
ddd = str(randint(10, 50))


class TestDialRestriction(unittest.TestCase):

    def setUp(self):
        self.driver = webdriver.Firefox(options=cases.options)
        self.driver.implicitly_wait(3)

    def test_create_dial_restriction(self):
        driver = self.driver
        cases.login(driver)
        driver.get("http://177.70.21.124/crm/index.php?codmodulo=487")
        driver.find_element_by_xpath(
            "//button[@id='btn_novo']/span/span").click()
        driver.find_element_by_id("uf").send_keys(ddd)
        driver.find_element_by_xpath(
            "//dd[@id='sctLdia_inicio']/div/div/a").click()
        driver.find_element_by_link_text("Segunda").click()
        driver.find_element_by_xpath(
            "//dd[@id='sctLdia_fim']/div/div/a").click()
        driver.find_element_by_xpath(
            "(//a[contains(text(),'Quarta')])[2]").click()
        driver.find_element_by_id("horario_inicio").send_keys("10:20")
        driver.find_element_by_id("horario_fim").send_keys("22:00")
        driver.save_screenshot(__name__+"A.png")
        time.sleep(1)
        driver.find_element_by_xpath(
            "//button[@id='btn_gravar']/span/span").click()
        time.sleep(3)
        WebDriverWait(driver, 10).until(
            EC.invisibility_of_element_located(
                (By.CLASS_NAME, "blockUI blockOverlay"))
        )
        driver.find_element_by_id("f_uf").send_keys(ddd)
        driver.find_element_by_xpath(
            "//button[@id='btn_buscar']/span/span").click()
        driver.find_element_by_id("grid_pager_right").click()
        el = driver.find_element_by_id("grid")\
            .find_element_by_tag_name("tbody")\
            .find_elements_by_tag_name("tr")[1]
        action = ActionChains(driver)
        action.double_click(el)
        action.perform()
        WebDriverWait(driver, 10).until(
            EC.visibility_of_element_located((By.ID, "uf"))
        )
        driver.save_screenshot(__name__+"B.png")
        try:
            res = cases.mse(self, __name__+"A.png", __name__+"B.png")
            self.assertAlmostEqual(0.0, res, delta=15.0)
        except AssertionError as e:
            driver.save_screenshot(cases.print_error(__name__))
            cases.logging.error(
                "Ocorreu algum erro ao criar uma pausa: " + str(e))
            raise AssertionError(
                "Ocorreu algum erro ao criar uma pausa: " + str(e))

    def test_delete_dial_restriction(self):
        driver = self.driver
        cases.login(driver)
        driver.get("http://177.70.21.124/crm/index.php?codmodulo=487")
        driver.find_element_by_id("f_uf").send_keys(ddd)
        driver.find_element_by_xpath(
            "//button[@id='btn_buscar']/span/span").click()
        time.sleep(2)
        el = driver.find_element_by_id("grid")\
            .find_element_by_tag_name("tbody")\
            .find_elements_by_tag_name("tr")[1]
        action = ActionChains(driver)
        action.double_click(el)
        action.perform()
        WebDriverWait(driver, 10).until(
            EC.visibility_of_element_located((By.ID, "uf"))
        )
        driver.find_element_by_xpath(
            "//button[@id='btn_deletar']/span/span").click()
        time.sleep(1)
        action = ActionChains(driver)
        action.key_down(Keys.ENTER)
        action.perform()
        time.sleep(1)

        try:
            res = driver.find_element_by_class_name("ui-paging-info").text
            self.assertEqual(res, "Nenhum registro para visualizar")
        except AssertionError as e:
            driver.save_screenshot(cases.print_error(__name__))
            cases.logging.error(
                "Ocorreu algum erro ao criar uma pausa: " + str(e))
            raise AssertionError(
                "Ocorreu algum erro ao criar uma pausa: " + str(e))

    def tearDown(self):
        self.driver.quit()


if __name__ == "__main__":
    unittest.main()
