from selenium import webdriver
import unittest
import time
import cases
from random import randint
from selenium.webdriver import ActionChains
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC


class TestUra(unittest.TestCase):
    def setUp(self):
        self.driver = webdriver.Firefox(options=cases.options)
        self.driver.implicitly_wait(3)

    def test_create_ura(self):
        randUra = randint(1, 999)
        try:
            driver = self.driver
            cases.login(driver)
            driver.get("http://{}/crm/index.php?codmodulo=141".format(cases.IP))
            time.sleep(1)
            driver.find_element_by_xpath(
                '//*[@id="btn_novo"]/span/span').click()
            driver.find_element_by_id('descricao').send_keys(
                'Ura Teste {}'.format(randUra))
            driver.find_element_by_xpath(
                '/html/body/div[3]/div[2]/div[1]/div[2]/form/fieldset[2]/dl/div[3]/dd/div/div/a').click()
            driver.find_element_by_xpath(
                '/html/body/div[3]/div[2]/div[1]/div[2]/form/fieldset[2]/dl/div[3]/dd/div/ul/li[5]/a').click()
            driver.find_element_by_id('tempoUra').clear()
            driver.find_element_by_id('tempoUra').send_keys('60')
            driver.find_element_by_xpath(
                '/html/body/div[3]/div[2]/div[1]/div[2]/form/fieldset[2]/dl/div[5]/dt/span/a').click()
            driver.find_element_by_xpath(
                '//*[@id="boxpermitir_ramal"]/span/a').click()
            # Adicionando a regra Movel-Local
            time.sleep(2)
            driver.find_element_by_id('regra_ramal').clear()
            driver.find_element_by_id('regra_ramal').send_keys('00ZX[789]X')
            time.sleep(1)
            driver.save_screenshot(__name__ + 'A.png')
            driver.find_element_by_xpath('/html/body/div[3]/div[2]/div[1]/div[2]/form/fieldset[1]/div[1]/button/span/span').click()
            WebDriverWait(driver, 10).until(
                EC.invisibility_of_element_located(
                    (By.CLASS_NAME, "blockUI blockOverlay"))
            )
            time.sleep(3)
            driver.find_element_by_xpath('/html/body/div[3]/div[2]/div[1]/div[2]/form/fieldset[1]/div[1]/button/span').click()
            WebDriverWait(driver, 10).until(
                EC.invisibility_of_element_located(
                    (By.CLASS_NAME, "blockUI blockOverlay"))
            )
            driver.find_element_by_xpath(
                '/html/body/div[3]/div[2]/div[1]/div[1]/div/div/div[5]/div/table/tbody/tr/td[2]/table/tbody/tr/td[6]/span').click()
            time.sleep(1)
            # Acessando a URA recém criada.
            grid_element_assert = driver.find_element_by_id("registros_listar")\
                .find_element_by_tag_name("tbody")\
                .find_elements_by_tag_name("tr")[-1]\
                .find_elements_by_tag_name("td")[-1]
            action = ActionChains(self.driver)
            action.double_click(grid_element_assert)
            action.perform()
            time.sleep(1)
            # Salvando o print B
            driver.save_screenshot(__name__ + 'B.png')
            time.sleep(1)
            driver.find_element_by_xpath(
                '/html/body/div[3]/div[2]/div[1]/div[2]/form/fieldset[1]/div[3]/div/button/span/span').click()
        except Exception as e:
            driver.save_screenshot(cases.print_error(__name__))
            cases.logging.error(__name__ + str(e))
        try:
            res = cases.mse(self, 'test_interface_uraA.png',
                            'test_interface_uraB.png')
            self.assertAlmostEqual(0.0, float(res), delta=500.00)
        except AssertionError as e:
            driver.save_screenshot(cases.print_error(__name__))
            cases.logging.error(__name__ + str(e))
            raise AssertionError(str(e))
        # Adiciona opções a uma URA já criada.
    '''
    def test_adding_and_removing_ura_options(self):
        try:
            driver = self.driver
            cases.login(driver)
            driver.get("http://{}/crm/index.php?codmodulo=141".format(cases.IP))
            time.sleep(2)
            grid_element_assert = driver.find_element_by_id("registros_listar")\
                .find_element_by_tag_name("tbody")\
                .find_elements_by_tag_name("tr")[-1]\
                .find_elements_by_tag_name("td")[-1]
            action = ActionChains(self.driver)
            action.double_click(grid_element_assert)
            action.perform()
            time.sleep(1)
            driver.save_screenshot(__name__+'3.png')
            driver.find_element_by_xpath(
                '/html/body/div[3]/div[2]/div[1]/div[2]/form/fieldset[1]/div[2]/button/span').click()
            driver.find_element_by_id('op_opcao').send_keys(str(randint(0, 9)))
            driver.find_element_by_id('op_descricao').send_keys('Timeout')
            driver.find_element_by_xpath(
                '/html/body/div[49]/div[2]/form/dl/div[3]/dd/div/div/div/a').click()
            driver.find_element_by_xpath(
                '/html/body/div[49]/div[2]/form/dl/div[3]/dd/div/div/ul/li[8]/a').click()
            time.sleep(1)
            driver.find_element_by_xpath(
                '/html/body/div[49]/div[3]/div/button[1]/span').click()
            time.sleep(5)
            # Deletando uma opção da URA
            grid_element_assert_options = driver.find_element_by_id("gview_opcoesgrid")\
                .find_element_by_tag_name("tbody")\
                .find_elements_by_tag_name("tr")[-1]\
                .find_elements_by_tag_name("td")[-1]
            action = ActionChains(self.driver)
            action.double_click(grid_element_assert_options)
            action.perform()
            time.sleep(1)
            driver.find_element_by_xpath(
                '/html/body/div[49]/div[3]/div/button[2]').click()
            time.sleep(1)
            driver.find_element_by_xpath(
                '/html/body/div[53]/div[3]/div/button[1]').click()
            time.sleep(4)
            driver.save_screenshot(__name__+'4.png')
        except Exception as e:
            driver.save_screenshot(cases.print_error(__name__))
            cases.logging.error(__name__ + str(e))

        try:
            res = cases.mse(self,
                            'test_interface_ura3.png',
                            'test_interface_ura4.png')
            self.assertAlmostEqual(0.0, float(res), delta=35.0)
        except AssertionError as e:
            driver.save_screenshot(cases.print_error(__name__))
            cases.logging.error(__name__ + str(e))
            raise AssertionError(str(e))
    '''

    def tearDown(self):
        self.driver.quit()


if __name__ == "__main__":
    unittest.main()
