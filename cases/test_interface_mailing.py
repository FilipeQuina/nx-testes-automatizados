# -*- coding: utf-8 -*-
from selenium import webdriver
from selenium.webdriver.support.ui import Select
import unittest
from time import sleep
import cases
from random import randint
from selenium.webdriver import ActionChains
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import os

mailing = "mailing" + str(randint(0, 1000))


class TestMailing(unittest.TestCase):

    def setUp(self):
        self.driver = webdriver.Firefox(options=cases.options)
        self.driver.implicitly_wait(3)

    def test_create_mailing(self):

        driver = self.driver
        cases.login(driver)
        driver.get("http://{}/crm/index.php?codmodulo=123".format(cases.IP))
        driver.find_element_by_xpath(
            "//div[@id='lbl_f_tipo_campanha']/dd/div/div/a").click()
        driver.find_element_by_link_text("LEAD").click()
        driver.find_element_by_xpath(
            "//button[@id='btn_pesquisar']/span/span").click()
        sleep(2)
        WebDriverWait(driver, 10).until(
            EC.visibility_of_all_elements_located(
                (By.ID, "registros_listar")
            )
        )
        el = driver.find_element_by_id("registros_listar")\
            .find_element_by_tag_name("tbody")\
            .find_elements_by_tag_name("tr")[1]
        id_campaign = el.find_elements_by_tag_name("td")[1]\
            .get_attribute("innerHTML")
        driver.get("http://{}/crm/index.php?codmodulo=126"
                   .format(cases.IP))
        driver.find_element_by_id("layout").click()
        Select(driver.find_element_by_id("layout"))\
            .select_by_visible_text("Campanha Fixa - 15")
        driver.find_element_by_id("arquivo").send_keys(
            os.path.abspath("teste.csv"))
        WebDriverWait(driver, 10).until(
            EC.element_to_be_clickable(
                (By.ID, "btnImportar"))
        )
        driver.find_element_by_id("btnImportar").click()
        WebDriverWait(driver, 10).until(
            EC.element_to_be_clickable(
                (By.XPATH, "//button[@onclick='adicionarFiltro();']"))
        )
        driver.find_element_by_xpath(
            "//button[@onclick='adicionarFiltro();']").click()
        Select(driver.find_element_by_id("campanha1")
               ).select_by_value(id_campaign)
        driver.find_element_by_id("nome_carga1").send_keys(mailing)
        driver.find_element_by_id("btnImportar2").click()
        WebDriverWait(driver, 20).until(
            EC.visibility_of_element_located(
                (By.ID, "mensagemSucesso"))
        )
        driver.get("http://{}/crm/index.php?codmodulo=123".format(cases.IP))
        driver.find_element_by_xpath(
            "//div[@id='lbl_f_tipo_campanha']/dd/div/div/a").click()
        driver.find_element_by_link_text("LEAD").click()
        driver.find_element_by_xpath(
            "//button[@id='btn_pesquisar']/span/span").click()
        WebDriverWait(driver, 10).until(
            EC.visibility_of_all_elements_located(
                (By.ID, "registros_listar")
            )
        )
        el_assert = driver.find_element_by_id("registros_listar")\
            .find_element_by_tag_name("tbody")\
            .find_elements_by_tag_name("tr")[1]
        action = ActionChains(self.driver)
        action.double_click(el_assert)
        action.perform()
        WebDriverWait(driver, 10).until(
            EC.element_to_be_clickable(
                (By.ID, "btn_carga"))
        )
        driver.find_element_by_id("btn_carga").click()
        sleep(2)
        mailing_assert = driver.find_element_by_id("grid_play_mailing")\
            .find_element_by_tag_name("tbody")\
            .find_elements_by_tag_name("tr")[1]\
            .find_elements_by_tag_name("td")[11]\
            .get_attribute("innerHTML")

        try:
            self.assertEqual(mailing, mailing_assert)
        except AssertionError as e:
            driver.save_screenshot(cases.print_error(__name__))
            cases.logging.error(__name__ + str(e))
            raise AssertionError(str(e))

    def tearDown(self):
        self.driver.quit()


if __name__ == "__main__":
    unittest.main()
