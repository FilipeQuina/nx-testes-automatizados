# -*- coding: utf-8 -*-
from selenium import webdriver
import unittest
import time
import cases
from random import randint
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import os

tabulation = u"tabulacao" + str(randint(0, 1000))


class TestBlackList(unittest.TestCase):

    def setUp(self):
        self.driver = webdriver.Firefox(options=cases.options)
        self.driver.implicitly_wait(3)

    def test_include_number_at_blackist(self):
        driver = self.driver
        try:
            driver = self.driver
            cases.login(driver)
            driver.get("http://{}/crm/index.php?codmodulo=94".format(cases.IP))

            driver.find_element_by_id("arquivo").send_keys(
                str(os.path.abspath("blacklist.csv")))
            driver.find_element_by_xpath(
                "//form[@id='frmModBlacklistImportar']/fieldset[2]/dl[2]/dd/div/div/a").click()
            driver.find_element_by_link_text("Importar Telefone").click()
            driver.find_element_by_id("btn_gravar").click()
            driver.find_element_by_css_selector(".swal2-confirm").click()
            time.sleep(2)
            WebDriverWait(driver, 10).until(
                EC.invisibility_of_element_located(
                    (By.CLASS_NAME, "blockUI blockOverlay"))
            )
            driver.find_element_by_id("btn_rel").click()
            driver.switch_to_frame("ext_iframe_blacklist")
            driver.find_element_by_id("f_telefone").send_keys("3232323232")
            driver.find_element_by_id("btn_pesquisar").click()
            WebDriverWait(driver, 10).until(
                EC.text_to_be_present_in_element(
                    (By.CLASS_NAME, "ui-paging-info"), "Ver 1 - 1 de 1")
            )
            tel_blk_lst = driver.find_element_by_id("registros_listar")\
                .find_element_by_tag_name("tbody")\
                .find_elements_by_tag_name("tr")[1]\
                .find_elements_by_tag_name("td")[2]\
                .get_attribute("innerHTML")

            self.assertEqual("3232323232", tel_blk_lst)
        except AssertionError as e:
            driver.save_screenshot(cases.print_error(__name__))
            cases.logging.error(__name__ + str(e))
            raise AssertionError(str(e))

    def test_remove_number_at_blackist(self):
        driver = self.driver
        try:
            driver = self.driver
            cases.login(driver)
            driver.get("http://{}/crm/index.php?codmodulo=94".format(cases.IP))
            driver.find_element_by_id("arquivo").send_keys(
                str(os.path.abspath("blacklist.csv")))
            driver.find_element_by_xpath(
                "//form[@id='frmModBlacklistImportar']/fieldset[2]/dl[2]/dd/div/div/a").click()
            driver.find_element_by_link_text("Remover Telefone").click()
            driver.find_element_by_id("btn_gravar").click()
            driver.find_element_by_css_selector(".swal2-confirm").click()
            time.sleep(3)
            WebDriverWait(driver, 10).until(
                EC.invisibility_of_element_located(
                    (By.CLASS_NAME, "blockUI blockOverlay"))
            )
            driver.find_element_by_id("btn_rel").click()
            driver.switch_to_frame("ext_iframe_blacklist")
            driver.find_element_by_id("f_telefone").send_keys("3232323232")
            driver.find_element_by_id("btn_pesquisar").click()
            time.sleep(1)
            driver.find_element_by_class_name("ui-paging-info")
            self.assertEqual("Nenhum registro para visualizar",
                             driver.find_element_by_class_name("ui-paging-info").text)
        except AssertionError as e:
            driver.save_screenshot(cases.print_error(__name__))
            cases.logging.error(__name__ + str(e))
            raise AssertionError(str(e))

    def tearDown(self):
        self.driver.quit()


if __name__ == "__main__":
    unittest.main()
