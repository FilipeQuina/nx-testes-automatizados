# -*- coding: utf-8 -*-
from selenium import webdriver
import unittest
import time
import cases
from random import randint
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver import ActionChains


ramal = str(randint(0, 1000)).zfill(4)


class TestRamal(unittest.TestCase):

    def setUp(self):
        self.driver = webdriver.Firefox(options=cases.options)
        self.driver.implicitly_wait(3)

    def test_create_ramal(self):
        driver = self.driver
        try:
            cases.login(driver)
            driver.get("http://{}/crm/index.php?codmodulo=344"
                       .format(cases.IP))
            driver.find_element_by_xpath(
                "//button[@id='btn_novo']/span/span").click()
            WebDriverWait(driver, 10).until(
                EC.visibility_of_element_located(
                    (By.ID, "lbl_idmostrar"))
            )
            driver.find_element_by_xpath(
                "//div[@id='lbl_id_tipo_ramal']/dd/div/div/a").click()
            driver.find_element_by_xpath(
                "(//a[contains(text(),'SIP')])[2]").click()
            driver.find_element_by_id("ramal").send_keys(ramal)
            driver.find_element_by_xpath(
                "//div[@id='lbl_nat']/dd/div/div/a").click()
            driver.find_element_by_link_text("Sim").click()
            driver.find_element_by_id("nome").send_keys(ramal)
            driver.find_element_by_id("permit").clear()
            driver.find_element_by_id("permit").send_keys("0.0.0.0/0.0.0.0")
            driver.find_element_by_xpath(
                "//div[@id='lbl_call_limit']/dd/div/div/a").click()
            driver.find_element_by_link_text("3").click()
            driver.find_element_by_id("btn_gravar").click()
            time.sleep(1)
            WebDriverWait(driver, 10).until(
                EC.invisibility_of_element(
                    (By.CLASS_NAME, "blockUI blockOverlay"))
            )
            driver.find_element_by_id("f_ramal").send_keys(ramal)
            driver.find_element_by_id("btn_pesquisar").click()
            WebDriverWait(driver, 10).until(
                EC.text_to_be_present_in_element(
                    (By.CLASS_NAME, "ui-paging-info"), "Ver 1 - 1 de 1")
            )
            ramal_assert = driver.find_element_by_css_selector(
                "#registros_listar > tbody:nth-child(1)")\
                .find_elements_by_tag_name("tr")[1]\
                .find_elements_by_tag_name("td")[4].get_attribute("innerHTML")
        except Exception as e:
            driver.save_screenshot(cases.print_error(__name__))
            cases.logging.error(__name__ + str(e))

        try:
            self.assertEqual(ramal, ramal_assert)
        except AssertionError as e:
            driver.save_screenshot(cases.print_error(__name__))
            cases.logging.error(__name__ + str(e))
            raise AssertionError(str(e))

    def test_remove_ramal(self):
        driver = self.driver
        try:
            cases.login(driver)
            driver.get("http://{}/crm/index.php?codmodulo=344"
                       .format(cases.IP))
            driver.find_element_by_id("f_ramal").send_keys(ramal)
            driver.find_element_by_id("btn_pesquisar").click()
            WebDriverWait(driver, 10).until(
                EC.text_to_be_present_in_element(
                    (By.CLASS_NAME, "ui-paging-info"), "Ver 1 - 1 de 1")
            )
            ramal_assert = driver.find_element_by_css_selector(
                "#registros_listar > tbody:nth-child(1)")\
                .find_elements_by_tag_name("tr")[1]\
                .find_elements_by_tag_name("td")[3]
            action = ActionChains(driver)
            action.double_click(ramal_assert)
            action.perform()
            WebDriverWait(driver, 10).until(
                EC.visibility_of_element_located(
                    (By.ID, "lbl_secret"))
            )
            driver.find_element_by_id("btn_deletar").click()
            driver.find_element_by_css_selector(
                ".ui-button.ui-widget.ui-state-default.ui-corner-all.ui-button-text-only").click()
            time.sleep(1)
            WebDriverWait(driver, 10).until(
                EC.invisibility_of_element(
                    (By.CLASS_NAME, "blockUI blockOverlay"))
            )
            driver.find_element_by_id("f_ramal").clear()
            driver.find_element_by_id("f_ramal").send_keys(ramal)
            driver.find_element_by_id("btn_pesquisar").click()
        except Exception as e:
            driver.save_screenshot(cases.print_error(__name__))
            cases.logging.error(__name__ + str(e))

        try:
            res = driver.find_element_by_class_name("ui-paging-info").text
            self.assertEqual(res, "Nenhum registro para visualizar")
        except AssertionError as e:
            driver.save_screenshot(cases.print_error(__name__))
            cases.logging.error(__name__ + str(e))
            raise AssertionError(str(e))

    def tearDown(self):
        self.driver.quit()


if __name__ == "__main__":
    unittest.main()
