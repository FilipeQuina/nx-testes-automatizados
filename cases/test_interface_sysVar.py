# -*- coding: utf-8 -*-
from selenium import webdriver
import unittest
import time
import cases
from selenium.webdriver import ActionChains
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC


class SysVar(unittest.TestCase):

    def setUp(self):
        self.driver = webdriver.Firefox(options=cases.options)
        self.driver.implicitly_wait(3)

    def test_validate_sys_var(self):
        try:
            driver = self.driver
            cases.login(driver)
            driver.get("http://{}/crm/index.php?codmodulo=321"
                       .format(cases.IP))
            driver.find_element_by_id("f_tag").send_keys("f_url_apismartnx")
            driver.find_element_by_id("btn_pesquisar").click()
            time.sleep(1)
            grid_element = driver.find_element_by_id("registros_listar")\
                .find_element_by_tag_name("tbody")\
                .find_elements_by_tag_name("tr")[1]\
                .find_elements_by_tag_name("td")[1]
            action = ActionChains(self.driver)
            action.double_click(grid_element)
            action.perform()
        except Exception as e:
            driver.save_screenshot(cases.print_error(__name__))
            cases.logging.error(__name__ + str(e))
        try:
            time.sleep(1)
            text_sys = driver.find_element_by_id(
                "valor").get_attribute("value")
            self.assertEqual(text_sys, "https://smart-api.smartnx.io/api")
        except AssertionError as e:
            driver.save_screenshot(cases.print_error(__name__))
            cases.logging.error(__name__ + str(e))
            raise AssertionError(str(e))

    def test_edit_sys_var(self):
        try:
            driver = self.driver
            cases.login(driver)
            driver.get("http://{}/crm/index.php?codmodulo=321"
                       .format(cases.IP))
            driver.find_element_by_id("f_tag").send_keys("f_url_apismartnx")
            driver.find_element_by_id("btn_pesquisar").click()
            WebDriverWait(driver, 5).until(
                EC.text_to_be_present_in_element(
                    (By.CLASS_NAME, "ui-paging-info"), "Ver 1 - 1 de 1")
            )
            grid_element = driver.find_element_by_id("registros_listar")\
                .find_element_by_tag_name("tbody")\
                .find_elements_by_tag_name("tr")[1]\
                .find_elements_by_tag_name("td")[1]
            action = ActionChains(self.driver)
            action.double_click(grid_element)
            action.perform()
            time.sleep(1)
            driver.find_element_by_id("boxstatus").click()
            driver.find_element_by_id("obs").clear()
            driver.find_element_by_id("obs").send_keys("--teste alteracao--")
            driver.save_screenshot(__name__+"A.png")
            time.sleep(1)
            driver.find_element_by_id("btn_gravar").click()
            WebDriverWait(driver, 10).until(
                EC.invisibility_of_element(
                    (By.CLASS_NAME, "blockUI blockOverlay"))
            )
            grid_element_assert = driver.find_element_by_id("registros_listar")\
                .find_element_by_tag_name("tbody")\
                .find_elements_by_tag_name("tr")[1]\
                .find_elements_by_tag_name("td")[1]
            action = ActionChains(self.driver)
            action.double_click(grid_element_assert)
            action.perform()
            time.sleep(1)
            driver.save_screenshot(__name__+"B.png")
        except Exception as e:
            driver.save_screenshot(cases.print_error(__name__))
            cases.logging.error(__name__ + str(e))
        try:
            res = cases.mse(
                self, __name__+"A.png", __name__+"B.png")
            self.assertAlmostEqual(0.0, float(res), delta=6.0)
        except AssertionError as e:
            driver.save_screenshot(cases.print_error(__name__))
            cases.logging.error(__name__ + str(e))
            raise AssertionError(str(e))

    def tearDown(self):
        self.driver.quit()


if __name__ == "__main__":
    unittest.main()
